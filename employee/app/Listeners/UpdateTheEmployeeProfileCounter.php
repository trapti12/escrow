<?php

namespace App\Listeners;

use App\Events\EmployeeProfileVisited;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

class UpdateTheEmployeeProfileCounter
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Handle the event.
     *
     * @param  EmployeeProfileVisited $event
     * @return bool|void
     */
    public function handle(EmployeeProfileVisited $event)
    {
        
        // Don't count the self-visits
        if (Auth::check()) {
            if (Auth::user()->id == $event->profile->user_id) {
                return false;
            }
        }

		if (!session()->has('EmployeeProfileVisited')) {
			return $this->updateCounter($event->profile);
		} else {
			if (session()->get('EmployeeProfileVisited') != $event->profile->id) {
				return $this->updateCounter($event->profile);
			} else {
				return false;
			}
		}
    }

	/**
	 * @param $profile
	 */
	public function updateCounter($profile)
	{
        $profile->visits = $profile->visits + 1;
        
		$profile->save(['canBeSaved' => true]);
		session()->put('EmployeeProfileVisited', $profile->id);
	}
}

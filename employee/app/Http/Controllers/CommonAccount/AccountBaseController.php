<?php
    

namespace App\Http\Controllers\CommonAccount;

use App\Http\Controllers\FrontController;
use App\Models\EmployeeProfile;
use App\Models\EmployeeMessage;
use App\Models\Message;
use App\Models\SavedPost;
use App\Models\SavedSearch;
use App\Models\Scopes\VerifiedScope;
use App\Models\Scopes\ReviewedScope;
use App\Helpers\Localization\Helpers\Country as CountryLocalizationHelper;
use App\Helpers\Localization\Country as CountryLocalization;
use Illuminate\Support\Facades\Auth;

abstract class AccountBaseController extends FrontController
{
    public $countries;
    public $myPosts;
    public $myEmployeeProfile;
    public $archivedPosts;
    public $favouritePosts;
    public $pendingPosts;
	public $conversations;
	public $transactions;
	public $companies;
	public $resumes;

    /**
     * AccountBaseController constructor.
     */
    public function __construct()
    {
        parent::__construct();
		
        $this->middleware(function ($request, $next) {
            $this->leftMenuInfo();
            return $next($request);
        });
	
		view()->share('pagePath', '');
    }

    public function leftMenuInfo()
    {
		// Get & Share Countries
        $this->countries = CountryLocalizationHelper::transAll(CountryLocalization::getCountries());
        view()->share('countries', $this->countries);
	
		// Share User Info
		view()->share('user', auth()->user());        

        // My Employee Profiles 
        $this->myEmployeeProfile = EmployeeProfile::currentCountry()
        ->where('user_id', auth()->user()->id)
         ->orderByDesc('id');
        view()->share('countMyEmployeeProflie', $this->myEmployeeProfile->count());        

        // Favourite Posts
        $this->favouritePosts = SavedPost::whereHas('post', function($query) {
                $query->currentCountry();
            })
            ->where('user_id', auth()->user()->id)
            ->with('post.city')
            ->orderByDesc('id');
        view()->share('countFavouritePosts', $this->favouritePosts->count());        

        // Save Search
        $savedSearch = SavedSearch::currentCountry()
            ->where('user_id', auth()->user()->id)
            ->orderByDesc('id');
        view()->share('countSavedSearch', $savedSearch->count());
	
		// Conversations
		$this->conversations = Message::whereHas('post', function($query) {
				$query->currentCountry();
			})->where(function($query) {
				$query->where('to_user_id', auth()->user()->id)->orWhere('from_user_id', auth()->user()->id);
			})
			->where('parent_id', 0)
			->where(function($query) {
				$query->where('deleted_by', '!=', auth()->user()->id)->orWhereNull('deleted_by');
			})
			->orderByDesc('id');
		view()->share('countConversations', $this->conversations->count());
		
    }
}

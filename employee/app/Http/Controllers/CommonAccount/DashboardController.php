<?php


namespace App\Http\Controllers\CommonAccount;

use App\Http\Controllers\Auth\Traits\VerificationTrait;
use App\Http\Requests\UserRequest;
use App\Models\Scopes\VerifiedScope;
use App\Models\UserType;
use Creativeorange\Gravatar\Facades\Gravatar;
use App\Models\Post;
use App\Models\SavedPost;
use App\Models\Gender;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Torann\LaravelMetaTags\Facades\MetaTag;
use App\Helpers\Localization\Helpers\Country as CountryLocalizationHelper;
use App\Helpers\Localization\Country as CountryLocalization;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends AccountBaseController
{
    use VerificationTrait;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = [];

        $data['countries'] = CountryLocalizationHelper::transAll(CountryLocalization::getCountries());
        $data['genders'] = Gender::trans()->get();
        $data['userTypes'] = UserType::all();
		$data['gravatar'] = (!empty(Auth::user()->email)) ? Gravatar::fallback(url('images/user.jpg'))->get(Auth::user()->email) : null;

        // Mini Stats
        $data['countPostsVisits'] = DB::table('posts')
            ->select('user_id', DB::raw('SUM(visits) as total_visits'))
            ->where('country_code', config('country.code'))
            ->where('user_id', Auth::user()->id)
            ->groupBy('user_id')
            ->first();

        $data['countPosts'] = Post::currentCountry()
            ->where('user_id', Auth::user()->id)
            ->count();

        $data['countFavoritePosts'] = SavedPost::whereHas('post', function($query) {
                $query->currentCountry();
            })->where('user_id', Auth::user()->id)
            ->count();

        //Only first profile stats will be queried,
        if( Auth::user()->user_type_id == 3) {
            $data['countProfilesVisits'] = DB::table('ee_profile')
                ->select('user_id', DB::raw('SUM(visits) as total_visits'))
                ->where('country_code', config('country.code'))
                ->where('user_id', Auth::user()->id)
                ->groupBy('user_id')
                ->first();
        }  
        
        if( Auth::user()->user_type_id == 3) {
            $data['countProfilesDownloads'] = DB::table('ee_profile')
                ->select('user_id', DB::raw('SUM(downloads) as total_downloads'))
                ->where('country_code', config('country.code'))
                ->where('user_id', Auth::user()->id)
                ->groupBy('user_id')
                ->first();
        }

        if( Auth::user()->user_type_id == 3) {
            /* $data['feed'] = DB::table('posts')
                ->select('user_id', DB::raw('SUM(downloads) as total_downloads'))
                ->where('country_code', config('country.code'))
                ->where('user_id', Auth::user()->id)
                ->groupBy('post_id')
                ->first(); */
        }

        // Meta Tags
        MetaTag::set('title', t('My account'));
        MetaTag::set('description', t('My account on :app_name', ['app_name' => config('settings.app.name')]));
        
        return view('account.common.dashboard', $data);
    }


   
}

<?php

namespace App\Http\Controllers\ProfileSearch;


use App\Helpers\ProfileSearch;
use App\Http\Controllers\ProfileSearch\Traits\PreSearchTrait;
use Illuminate\Support\Facades\Input;
use Torann\LaravelMetaTags\Facades\MetaTag;
use App\Models\EmployeeProfile;
use Config;

class SearchController extends BaseController
{
    use PreSearchTrait;

	public $isIndexSearch = true;

    protected $cat = null;
    protected $subCat = null;
    protected $city = null;
    protected $admin = null;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        view()->share('isIndexSearch', $this->isIndexSearch);

        // Pre-Search
        if (Input::filled('c')) {
            if (Input::filled('sc')) {
                $this->getCategory(Input::get('c'), Input::get('sc'));
            } else {
                $this->getCategory(Input::get('c'));
            }
        }
        if (Input::filled('l') || Input::filled('location')) {
            $city = $this->getCity(Input::get('l'), Input::get('location'));
        }
        if (Input::filled('r') && !Input::filled('l')) {
            $admin = $this->getAdmin(Input::get('r'));
        }

        // Pre-Search values
        $preSearch = [
            'city'  => (isset($city) && !empty($city)) ? $city : null,
            'admin' => (isset($admin) && !empty($admin)) ? $admin : null,
        ];

        // Search
        $search = new ProfileSearch($preSearch);
        $data = $search->fechAll();

        //echo '<pre>'; print_r($data['count']); echo '</pre>'; die;
        // Export Search Result
        view()->share('count', $data['count']);
        view()->share('paginator', $data['paginator']);

        // Get Titles
        $title = $this->getTitle();

        
        $this->getBreadcrumb();
        $this->getHtmlTitle();

        // Meta Tags
        MetaTag::set('title', $title);
        MetaTag::set('description', $title);

        return view('employee.serp');
    }
/* 
    public function employee_list($cat_slug = false){
         view()->share('isIndexSearch', $this->isIndexSearch);

        // Pre-Search
        if (Input::filled('c')) {
            if (Input::filled('sc')) {
                $this->getCategory(Input::get('c'), Input::get('sc'));
            } else {
                $this->getCategory(Input::get('c'));
            }
        }
        if (Input::filled('l') || Input::filled('location')) {
            $city = $this->getCity(Input::get('l'), Input::get('location'));
        }
        if (Input::filled('r') && !Input::filled('l')) {
            $admin = $this->getAdmin(Input::get('r'));
        }

        // Pre-Search values
        $preSearch = [
            'city'  => (isset($city) && !empty($city)) ? $city : null,
            'admin' => (isset($admin) && !empty($admin)) ? $admin : null,
        ];

        // Search
        $obj = new EmployeeProfile();
        $data = $obj->fechEmployeeAll($preSearch, $cat_slug);
$data = $data->appends(Input::except(array('page')));
        // Export Search Result
        view()->share('count', count($data));
        view()->share('data', $data);
        view()->share('salary_type', Config::get('params.salary_type'));
        view()->share('total_exp', Config::get('params.total_exp'));

        // Get Titles
       $title = "Employee list";
        $this->getBreadcrumb();
        $this->getHtmlTitle();

        // Meta Tags
        MetaTag::set('title', $title);
        MetaTag::set('description', $title);

        return view('employee.serp');
    }
    public function employee_details($employee_profile_id, $resume_heading){
             $obj = new EmployeeProfile();
            $data = $obj->getById($employee_profile_id);
            if (empty($data)) {
            abort(404);
        }
        view()->share('salary_type', Config::get('params.salary_type'));
        view()->share('total_exp', Config::get('params.total_exp'));
          view()->share('data', $data);
          return view('employee.details');
    } */
}

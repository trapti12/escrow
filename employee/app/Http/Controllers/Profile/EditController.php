<?php


namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Profile\Traits\EditTrait;
use App\Http\Controllers\Auth\Traits\VerificationTrait;
use App\Http\Requests\EmployeeRequest;
use App\Models\Company;
use App\Models\PostType;
use App\Models\Industry;
use App\Models\FunctionalArea;
use App\Models\Role;
use App\Models\Course;
use App\Models\Specialization;
use App\Models\SalaryType;
use App\Http\Controllers\FrontController;
use App\Helpers\Localization\Helpers\Country as CountryLocalizationHelper;
use App\Helpers\Localization\Country as CountryLocalization;
use Illuminate\Support\Facades\Auth;

class EditController extends FrontController
{
    use EditTrait, VerificationTrait;

    public $data;
    public $msg = [];
    public $uri = [];

    /**
     * EditController constructor.
     */
    public function __construct()
    {
        parent::__construct();
		
        $this->middleware(function ($request, $next) {
            $this->commonQueries();
            return $next($request);
        });
    }

    /**
     * Common Queries
     */
    public function commonQueries()
    {
        // References
        $data = [];
    
        // Get Countries
        $data['countries'] = $this->countries = CountryLocalizationHelper::transAll(CountryLocalization::getCountries());
        $this->countries = $data['countries'];
        view()->share('countries', $data['countries']);
    
        // Get Industries
        $data['industries'] = Industry::trans()->where('parent_id', 0)->with([
            'children' => function ($query) {
                $query->trans();
            },
        ])->orderBy('lft')->get();
        view()->share('industries', $data['industries']);

        // Get Functional Areas
        $data['functional_areas'] = FunctionalArea::trans()->get();
        view()->share('functional_areas', $data['functional_areas']);        

        // Get Roles
        $data['roles'] = Role::trans()->get();
        view()->share('roles', $data['roles']);

        // Get Courses
        $data['courses'] = Course::trans()->get();
        view()->share('courses', $data['courses']);

        // Get Specializations
        $data['specializations'] = Specialization::trans()->get();
        view()->share('specializations', $data['specializations']);
    
        // Get Post Types
        $data['postTypes'] = PostType::trans()->get();
        view()->share('postTypes', $data['postTypes']);
    
        // Get Salary Types
        $data['salaryTypes'] = SalaryType::trans()->get();
        view()->share('salaryTypes', $data['salaryTypes']);	
		
    
        // Save common's data
        $this->data = $data;
    }
    
    /**
     * Show the form the create a new profile.
     *
     * @param $profileId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getBasicForm($profileId)
    {
        return $this->getUpdateBasicForm($profileId);
    }
    
    /**
     * Store a new ad profile.
     *
     * @param $profileId
     * @param EmployeeProfileRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postBasicForm($profileId, EmployeeProfileRequest $request)
    {
        return $this->postUpdateBasicForm($profileId, $request);
    }

    /**
     * Edit Education
     *
     * @param $profileId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEducationForm($profileId)
    {
        return $this->getUpdateEducationForm($profileId);
    }
    
    /**
     * Store Education.
     *
     * @param $profileId
     * @param EmployeeProfileRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postEducationForm($profileId, EmployeeProfileRequest $request)
    {
        return $this->postUpdateEducationForm($profileId, $request);
    }

    /**
     * Edit Employment
     *
     * @param $profileId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEmploymentForm($profileId)
    {
        return $this->getUpdateEmploymentForm($profileId);
    }
    
    /**
     * Store Employment.
     *
     * @param $profileId
     * @param EmployeeProfileRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postEmploymentForm($profileId, EmployeeProfileRequest $request)
    {
        return $this->postUpdateEmploymentForm($profileId, $request);
    }

    /**
     * Edit Extra Details
     *
     * @param $profileId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getExtraForm($profileId)
    {
        return $this->getUpdateExtraForm($profileId);
    }
    
    /**
     * Store Extra Details
     *
     * @param $profileId
     * @param EmployeeProfileRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postExtraForm($profileId, EmployeeProfileRequest $request)
    {
        return $this->postUpdateExtraForm($profileId, $request);
    }

    /**
     * Edit Resume
     *
     * @param $profileId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getResumeForm($profileId)
    {
        return $this->getUpdateResumeForm($profileId);
    }
    
    /**
     * Store Resume.
     *
     * @param $profileId
     * @param EmployeeProfileRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postResumeForm($profileId, EmployeeProfileRequest $request)
    {
        return $this->postUpdateResumeForm($profileId, $request);
    }

    /**
     * Edit Photo
     *
     * @param $profileId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPhotoForm($profileId)
    {
        return $this->getUpdatePhotoForm($profileId);
    }
    
    /**
     * Store Photo.
     *
     * @param $profileId
     * @param EmployeeProfileRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postPhotoForm($profileId, EmployeeProfileRequest $request)
    {
        return $this->postUpdatePhotoForm($profileId, $request);
    }
}

<?php   

namespace App\Http\Controllers\Profile;

use App\Helpers\Arr;
use App\Helpers\Ip;
use App\Http\Controllers\Profile\Traits\EditTrait;
use App\Http\Controllers\Auth\Traits\VerificationTrait;
use App\Http\Requests\EmployeeProfileRequest;
use App\Http\Requests\EmployeeProfileExtraRequest;
use App\Models\EmployeeProfile;
use App\Models\EmployeeProfileEmployment;
use App\Models\EmployeeProfileEducation;
use App\Models\EmployeeProfileExtra;
use App\Models\EmployeeSettings;
use App\Models\PostType;
use App\Models\Category;
use App\Models\City;
use App\Models\SalaryType;
use App\Models\User;
use App\Http\Controllers\FrontController;
use App\Models\Scopes\VerifiedScope;
use App\Models\Scopes\ReviewedScope;
use App\Mail\EmployeeProfileNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use Torann\LaravelMetaTags\Facades\MetaTag;
use App\Helpers\Localization\Helpers\Country as CountryLocalizationHelper;
use App\Helpers\Localization\Country as CountryLocalization;

class CreateController extends FrontController
{
	use EditTrait, VerificationTrait;
	
	public $data;
	
	/**
	 * CreateController constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		
		
		// From Laravel 5.3.4 or above
		$this->middleware(function ($request, $next) {
			$this->commonQueries();
			
			return $next($request);
		});
	}
	
	/**
	 * Common Queries
	 */
	public function commonQueries()
	{
		// References
		$data = [];
		
		// Get Countries
		$data['countries'] = CountryLocalizationHelper::transAll(CountryLocalization::getCountries());
		view()->share('countries', $data['countries']);
		
		// Get Categories
		$cacheId = 'categories.parentId.0.with.children' . config('app.locale');
		$data['categories'] = Cache::remember($cacheId, $this->cacheExpiration, function () {
			$categories = Category::trans()->where('parent_id', 0)->with([
				'children' => function ($query) {
					$query->trans();
				},
			])->orderBy('lft')->get();
			
			return $categories;
		});

		view()->share('categories', $data['categories']);
        
		// Save common's data
		$this->data = $data;
	}
	
	/**
	 * New Post's Form.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getForm($ee_profile_type)
	{
		
		view()->share('resume_type', $ee_profile_type);
		// Check possible Update
		if (!empty($tmpToken)) {
			session()->keep(['message']);
			
			return $this->getUpdateForm($tmpToken);
		}
		
		// Meta Tags
		MetaTag::set('title', 'Add Your Employee Profile');
		MetaTag::set('description', strip_tags(getMetaTag('description', 'create')));
		MetaTag::set('keywords', getMetaTag('keywords', 'create'));
		
		// Create
		return view('profile.employee.create');
	}
	
	/**
	 * Store a new Profile.
	 *
	 * @param null $tmpToken
	 * @param EmployeeProfileRequest $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function postForm($tmpToken = null, EmployeeProfileRequest $request)
	{
		/* // Check possible update
		if (!empty($tmpToken)) {
			session()->keep(['message']);
			
			return $this->profileUpdateForm($tmpToken, $request);
		} */
		
		// Get the Profile's City
		/* $city = City::find($request->input('city', 0));
		if (empty($city)) {
			flash("Adding profile was disabled for this time. Please try later. Thank you.")->error();
			
			return back()->withInput();
		} */
		
		// Conditions to Verify User's Email or Phone
		if (Auth::check()) {
			$emailVerificationRequired = config('settings.mail.email_verification') == 1 && $request->filled('email') && $request->input('email') != Auth::user()->email;
			$phoneVerificationRequired = config('settings.sms.phone_verification') == 1 && $request->filled('phone') && $request->input('phone') != Auth::user()->phone;
		} else {
			$emailVerificationRequired = config('settings.mail.email_verification') == 1 && $request->filled('email');
			$phoneVerificationRequired = config('settings.sms.phone_verification') == 1 && $request->filled('phone');
		}	
		
		//id country_code	user_id	name	email	mobile	verified_email	verified_phone	email_token	phone_token	dob	marital_status	
		//gender	resume_headline	highest_qualification	total_experience industry_id	functional_area_id	role_id	current_location	
        //prefered_location	lon	lat	skills	current_salary	expected_salary	it_skills	profile_summary 
        //photo resume downloads	visits	archived ee_profile_type	
		//created_at	updated_at

		// Profile Basic Data
		$profileBasicInfo = [
			'country_code'          => config('country.code'),
			'user_id'               => Auth::user()->id,
			'profile_name'			=> $request->input('profile_name'),
			'name'                  => $request->input('name'),
			'email'   				=> $request->input('email'),
			'phone'   				=> $request->input('phone'),
			'verified_email'		=> 1,
			'verified_phone'		=> 1,
			'ee_profile_type'		=> $request->input('resume_type'),			        		
			'current_location'		=> $request->input('current_location'),
			'prefered_location'		=> $request->input('prefered_location'),			
			'highest_qualification'	=> $request->input('highest_qualification'),
			'skills'				=> $request->input('skills'),				
		];		

		// Email verification key generation
		if ($emailVerificationRequired) {
			$profileBasicInfo['email_token'] = md5(microtime() . mt_rand());
			$profileBasicInfo['verified_email'] = 0;
		}
		
		// Mobile activation key generation
		if ($phoneVerificationRequired) {
			$profileBasicInfo['phone_token'] = mt_rand(100000, 999999);
			$profileBasicInfo['verified_phone'] = 0;
		}

		$isExperienced = $request->input('resume_type');
		
		if($isExperienced == 1) {

			$profileBasicInfo['current_salary']       = $request->input('current_salary');
			$total_experience 						  = $request->input('year_experience').','.$request->input('month_experience');
			$profileBasicInfo['total_experience']     = $total_experience;
			$profileBasicInfo['notice_period']        = $request->input('notice_period');
			$profileBasicInfo['notice_period_status'] = $request->input('notice_period_status');
		}	

		// Save the Profile into database
		$profile = new EmployeeProfile($profileBasicInfo);
		$profile->save();

		if($request->hasFile('resume')){
			$profile->resume = $request->file('resume.filename');
			$profile->save();
		}
		
		// Save profile Id in session (for next steps)
		session(['tmpEmployeeProfileId' => $profile->id]);
		
		// Profile Employment Data
		if($isExperienced == 1) {

			$from = $request->input('from_month').','.$request->input('from_year');
			$to   = $request->input('to_month').','.$request->input('to_year');

			
			$profileEmploymentInfo = [				
				'ee_profile_id'       => session('tmpEmployeeProfileId'),
				'designation'         => $request->input('current_designation'),
				'company'             => $request->input('company'),
				'from_date'      	  => $from,
				'to_date'             => $to,
				'description'     	  => $request->input('job_description'),
				'working_status'      => $request->input('working_status'),										
			];
			// Save the Profile Employment data into database
			$profileEmployment = new EmployeeProfileEmployment($profileEmploymentInfo);
			$profileEmployment->save();

		}		

				

		// Profile Education Data
		$profileEducationInfo = [
			'ee_profile_id'       => session('tmpEmployeeProfileId'),
			'course'         	  => $request->input('course'),
			'specialization'      => $request->input('specialization'),
			'institute'      	  => $request->input('institute'),
			'course_type'      	  => $request->input('course_type'),
			'passing_year'        => $request->input('passing_year'),
		];

		/* echo "<pre>"; print_r($profileBasicInfo); echo "</pre>";
		echo "<pre>"; print_r($profileEmploymentInfo); echo "</pre>";
		echo "<pre>"; print_r($profileEducationInfo); echo "</pre>"; die; */
		// Save the Profile into database
		$profileEducation = new EmployeeProfileEducation($profileEducationInfo);
		$profileEducation->save();

		// $nextStepUrl = config('app.locale') . '/profiles/create/' . $profile->tmp_token . '/packages';
		// $nextStepUrl = config('app.locale') . '/profiles/create/' . $profile->tmp_token . '/finish';		
		
		$request->session()->flash('message', 'Your profile has been created.');
		$nextStepUrl = config('app.locale') . '/profile/additional';
		// Send Admin Notification Email
		if (config('settings.mail.admin_email_notification') == 1) {
			try {
				// Get all admin users
				$admins = User::where('is_admin', 1)->get();
				if ($admins->count() > 0) {
					foreach ($admins as $admin) {
						Mail::send(new EmployeeProfileNotification($post, $admin));
					}
				}
			} catch (\Exception $e) {
				flash($e->getMessage())->error();
			}
		}
		
		// Send Email Verification message
		if ($emailVerificationRequired) {
			// Save the Next URL before verification
			session(['itemNextUrl' => $nextStepUrl]);
			
			// Send
			$this->sendVerificationEmail($profile);
			
			// Show the Re-send link
			$this->showReSendVerificationEmailLink($profile, 'profile');
		}	

		//Total number of profiles submitted by the user
		$count = EmployeeProfile::where('user_id', Auth::user()->id)->count();

		//Default profile 
		if($count == 1) {
			$ee_settings = new EmployeeSettings;

			$ee_settings->user_id = Auth::user()->id;
			$ee_settings->default_profile_id = session('tmpEmployeeProfileId');
			
			//save values
			$ee_settings->save();
		}

		// Redirection
		return redirect($nextStepUrl);
	}
	
	/**
	 * Additional Profile Info's Form.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getExtraForm()
	{
				
		// Meta Tags
		MetaTag::set('title', 'Additional Profile Details');
		MetaTag::set('description', strip_tags(getMetaTag('description', 'create')));
		MetaTag::set('keywords', getMetaTag('keywords', 'create'));
		
		// Create
		return view('profile.employee.create_extra');
	}
	
	/**
	 * Store additional Profile details .
	 *
	 * @param null $tmpToken
	 * @param EmployeeProfileExtraRequest $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function postExtraForm($tmpToken = null, EmployeeProfileExtraRequest $request)
	{
		

		$dob = $request->input('dob_year')."-".$request->input('dob_month')."-".$request->input('dob_day');
		$address = $request->input('address').",".$request->input('address_city')."-".$request->input('address_pin');			

		$profileBasic = EmployeeProfile::where('id', session('tmpEmployeeProfileId'))->first();
		//echo "<pre>"; print_r($profileBasic); echo "</pre>"; die;		
		
		$profileBasic->profile_summary = $request->input('professional_background');
		$profileBasic->industry_id = $request->input('industry');
		$profileBasic->functional_area_id = $request->input('functional_area');
		$profileBasic->role_id = $request->input('role');
		
		// Save Profile Data
		$profileBasic->save();

		if($request->hasFile('photo')){
			$profileBasic->photo = $request->file('photo');
			$profileBasic->save();
		}

		// Profile Data 
		
		$profileExtraInfo = [
			'ee_profile_id'  			=> session('tmpEmployeeProfileId'),	
			'dob'                       => $dob,
			'address'  					=> $address,		
			'desired_job_type'      	=> $request->input('desired_job_type'),
			'desired_employment_type'   => $request->input('desired_employment_type'),			
			'work_permit_countries'     => $request->input('work_permit_countries'),			
			'disability'        		=> $request->input('disability')			
		];		

				
		
		// Save the Profile into database

		$profile = new EmployeeProfileExtra($profileExtraInfo);
		$profile->save();
				
		
		$request->session()->flash('message', t('Your ad has been created.'));
		$nextStepUrl = config('app.locale') . '/profile/finish/';

		// Redirection
		return redirect($nextStepUrl);
	}


	/**
	 * Confirmation
	 *
	 * @param $tmpToken
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
	 */
	public function finish()
	{
		/* // Keep Success Message for the page refreshing
		session()->keep(['message']);
		if (!session()->has('message')) {
			return redirect(config('app.locale') . '/');
		} */
		
		// Clear the steps wizard
		/* if (session()->has('tmpPostId')) {
			// Get the Post
			$post = Post::withoutGlobalScopes([VerifiedScope::class, ReviewedScope::class])->where('id', session('tmpPostId'))->where('tmp_token', $tmpToken)->first();
			if (empty($post)) {
				abort(404);
			}
			
			// Apply finish actions
			$post->tmp_token = null;
			$post->save();
			session()->forget('tmpPostId');
		} */
		
		// Redirect to the Post,
		// - If User is logged
		// - Or if Email and Phone verification option is not activated
		/* if (Auth::check() || (config('settings.mail.email_verification') != 1 && config('settings.sms.phone_verification') != 1)) {
			if (!empty($post)) {
				flash(session('message'))->success();
				
				return redirect(config('app.locale') . '/' . $post->uri);
			}
		} */
		
		// Meta Tags
		MetaTag::set('title', session('message'));
		MetaTag::set('description', session('message'));
		
		return view('profile.employee.finish');
	}
}

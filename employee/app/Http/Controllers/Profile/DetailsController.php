<?php

namespace App\Http\Controllers\Profile;

use App\Events\EmployeeProfileVisited;
use App\Helpers\Arr;
use App\Http\Requests\SendMessageRequest;
use App\Models\EmployeeProfile;
use App\Models\PostType;
use App\Models\Category;
use App\Models\City;
use App\Models\Message;
use App\Models\Package;
use App\Models\Payment;
use App\Http\Controllers\FrontController;
use App\Models\Resume;
use App\Models\SalaryType;
use App\Models\User;
use App\Models\Scopes\VerifiedScope;
use App\Models\Scopes\ReviewedScope;
use App\Notifications\EmployerContacted;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Date\Date;
use Larapen\TextToImage\Facades\TextToImage;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Torann\LaravelMetaTags\Facades\MetaTag;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Localization\Helpers\Country as CountryLocalizationHelper;
use App\Helpers\Localization\Country as CountryLocalization;

class DetailsController extends FrontController
{
	/**
	 * Profile expire time (in months)
	 *
	 * @var int
	 */
	public $expireTime = 24;
	
	/**
	 * DetailsController constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		
		// From Laravel 5.3.4 or above
		$this->middleware(function ($request, $next) {
			$this->commonQueries();
			
			return $next($request);
		});
	}
	
	/**
	 * Common Queries
	 */
	public function commonQueries()
	{
		// Check Country URL for SEO
		$countries = CountryLocalizationHelper::transAll(CountryLocalization::getCountries());
		view()->share('countries', $countries);
	}
	
	/**
	 * Show Profile's Details.
	 *
	 * @param $profileId
	 * @return View
	 */
	public function index($profileId)
	{
		$data = [];
		
		// Get and Check the Controller's Method Parameters
		$parameters = Request::route()->parameters();
		
		// Show 404 error if the Profile's ID is not numeric
		if (!isset($parameters['id']) || empty($parameters['id']) || !is_numeric($parameters['id'])) {
			abort(404);
		}
		
		// Set the Parameters
		$profileId = $parameters['id'];
		if (isset($parameters['slug'])) {
			$slug = $parameters['slug'];
		}
		//echo $profileId;
		// GET PROFILE'S DETAILS
		if (Auth::check()) {
			// Get profile's details even if it's not activated and reviewed
			$cacheId = 'profile.withoutGlobalScopes.with.user.city.pictures.' . $profileId;
			$profile = Cache::remember($cacheId, $this->cacheExpiration, function () use ($profileId) {
				$profile = EmployeeProfile::withoutGlobalScopes([VerifiedScope::class, ReviewedScope::class])->unarchived()->where('id', $profileId)->with(['user', 'city', 'pictures'])->first();
				
				return $profile;
			});
			
			// If the logged user is not an admin user...
			if (Auth::user()->is_admin != 1) {
				// Then don't get profile that are not from the user
				if (!empty($profile) && $profile->user_id != Auth::user()->id) {
					$cacheId = 'profile.with.user.city.pictures.' . $profileId;
					$profile = Cache::remember($cacheId, $this->cacheExpiration, function () use ($profileId) {
						$profile = EmployeeProfile::unarchived()->where('id', $profileId)->with(['user', 'city', 'pictures'])->first();
						
						return $profile;
					});
				}
			}
			
			// Get the User's Resumes
			$limit = config('larapen.core.selectResumeInto', 5);
			$cacheId = 'resumes.take.' . $limit . '.where.user.' . Auth::user()->id;
			$resumes = Cache::remember($cacheId, $this->cacheExpiration, function () use ($limit) {
				$resumes = Resume::where('user_id', Auth::user()->id)->take($limit)->orderByDesc('id')->get();
				
				return $resumes;
			});
			view()->share('resumes', $resumes);
			
			// Get the User's latest Resume
			if ($resumes->has(0)) {
				$lastResume = $resumes->get(0);
				view()->share('lastResume', $lastResume);
			}
		} else {
			$cacheId = 'profile.with.user.city.pictures.' . $profileId;
			$profile = Cache::remember($cacheId, $this->cacheExpiration, function () use ($profileId) {
				$profile = EmployeeProfile::unarchived()->where('id', $profileId)->with(['user', 'city', 'pictures'])->first();
				
				return $profile;
			});
		}
		//echo "<pre>"; print_r($profile); echo "</pre"; die;
		// Preview the Profile after activation
		if (Input::filled('preview') && Input::get('preview') == 1) {
			// Get profile's details even if it's not activated and reviewed
			$profile = EmployeeProfile::withoutGlobalScopes([VerifiedScope::class, ReviewedScope::class])->where('id', $profileId)->with(['user', 'city', 'pictures'])->first();
		}

		//echo "<pre>"; print_r($profile); echo "</pre>"; die;
		
		// profile not found
		if (empty($profile)) {
			abort(404, t('profile not found'));
		}
		
		// Share profile's details
		view()->share('profile', $profile);
		
		
		// Get category details
		$cacheId = 'category.' . $profile->category_id . '.' . config('app.locale');
		$cat = Cache::remember($cacheId, $this->cacheExpiration, function () use ($profile) {
			$cat = Category::transById($profile->category_id);
			
			return $cat;
		});
		view()->share('cat', $cat);
		
		// Get profile type details
		$cacheId = 'postType.' . $profile->job_type . '.' . config('app.locale');
		$postType = Cache::remember($cacheId, $this->cacheExpiration, function () use ($profile) {
			$postType = postType::transById($profile->job_type);
			
			return $postType;
		});
		view()->share('postType', $postType);
		
		// Get the profile's Salary Type
		/* $cacheId = 'salaryType.' . $profile->salary_type_id . '.' . config('app.locale');
		$salaryType = Cache::remember($cacheId, $this->cacheExpiration, function () use ($profile) {
			$salaryType = SalaryType::transById($profile->salary_type_id);
			
			return $salaryType;
		}); 
		view()->share('salaryType', $salaryType); */
		
		
		// Require info
		/*if (empty($cat) || empty($postType)) {
			abort(404);
		}*/
		
		
		// Get package details
		/* $package = null;
		if ($profile->featured == 1) {
			$payment = Payment::where('profile_id', $profile->id)->orderBy('id', 'DESC')->first();
			if (!empty($payment)) {
				$package = Package::transById($payment->package_id);
			}
		}
		view()->share('package', $package); */
		
		
		// Get ad's user decision about comments activation
		$commentsAreDisabledByUser = false;
		// Get possible ad's user
		if (isset($profile->user_id) && !empty($profile->user_id)) {
			$possibleUser = User::find($profile->user_id);
			if (!empty($possibleUser)) {
				if ($possibleUser->disable_comments == 1) {
					$commentsAreDisabledByUser = true;
				}
			}
		}
		view()->share('commentsAreDisabledByUser', $commentsAreDisabledByUser);
		
		
		// GET PARENT CATEGORY
		/*if ($cat->parent_id == 0) {
			$parentCat = $cat;
		} else {
			$parentCat = Category::transById($cat->parent_id);
		}
		view()->share('parentCat', $parentCat);*/
		
		// Increment profile visits counter
		Event::fire(new EmployeeProfileVisited($profile));
		
		// GET SIMILAR PROFILES
		$featured = $this->getCategorySimilarPosts($cat, $profile->id);
		// $featured = $this->getLocationSimilarPosts($post->city, $post->id);
		$data['featured'] = $featured;
		
		// SEO
		$title = $profile->profile_name . ', ' . $profile->current_location;
		$description = str_limit(str_strip(strip_tags($profile->description)), 200);
		
		// Meta Tags
		MetaTag::set('title', $title);
		MetaTag::set('description', $description);
		if (!empty($profile->tags)) {
			MetaTag::set('keywords', str_replace(',', ', ', $profile->tags));
		}
		
		// Open Graph
		$this->og->title($title)
				 ->description($description)
				 ->type('article')
				 ->article(['author' => config('settings.social_link.facebook_page_url')])
				 ->article(['publisher' => config('settings.social_link.facebook_page_url')]);
		if (!$profile->pictures->isEmpty()) {
			if ($this->og->has('image')) {
				$this->og->forget('image')->forget('image:width')->forget('image:height');
			}
			foreach ($profile->pictures as $picture) {
				$this->og->image(resize($picture->filename, 'large'), [
					'width'  => 600,
					'height' => 600,
				]);
			}
		}
		view()->share('og', $this->og);
		
		// Expiration Info
		$today_dt = Date::now(config('timezone.id'));
		if ($today_dt->gt($profile->created_at->addMonths($this->expireTime))) {
			flash(t("Warning! This ad has expired. The product or service is not more available (may be)"))->error();
		}
		
		//echo "<pre>"; print_r($data); echo "</pre>"; die;
		// View
		return view('profile.employee.details', $data);
	}
	
	/**
	 * @param $postId
	 * @param SendMessageRequest $request
	 * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function sendMessage($postId, SendMessageRequest $request)
	{
		$this->middleware('auth', ['only' => ['sendMessage']]);
		
		// Get the Post
		$post = Post::unarchived()->findOrFail($postId);
		
		// Get or Create Resume
		if ($request->filled('resume_id') && !empty($request->input('resume_id'))) {
			// Get the User's Resume
			$resume = Resume::where('id', $request->input('resume_id'))->where('user_id', Auth::user()->id)->first();
		} else {
			// Get form Requests
			$resumeInfo = $request->input('resume');
			$resumeInfo += ['active' => 1];
			if (!isset($resumeInfo['filename'])) {
				$resumeInfo += ['filename' => null];
			}
			if (!isset($resumeInfo['country_code']) || empty($resumeInfo['country_code'])) {
				$resumeInfo += ['country_code' => config('country.code')];
			}
			
			// Logged Users
			if (Auth::check()) {
				if (!isset($resumeInfo['user_id']) || empty($resumeInfo['user_id'])) {
					$resumeInfo += ['user_id' => Auth::user()->id];
				}
				
				// Store the User's Resume
				$resume = new Resume($resumeInfo);
				$resume->save();
				
				// Save the Resume's file
				if ($request->hasFile('resume.filename')) {
					$resume->filename = $request->file('resume.filename');
					$resume->save();
				}
			} else {
				// Guest Users
				$resume = Arr::toObject($resumeInfo);
			}
		}
		
		// Return error if resume is not set
		if (empty($resume)) {
			flash(t("Please select a resume or 'New Resume' to add one."))->error();
			
			return back()->withInput($request->except('resume.filename'));
		}
		
		// Store Message
		$message = new Message([
			'post_id'      => $post->id,
			'from_user_id' => Auth::check() ? Auth::user()->id : 0,
			'from_name'    => $request->input('name'),
			'from_email'   => $request->input('email'),
			'from_phone'   => $request->input('phone'),
			'to_user_id'   => $post->user_id,
			'to_name'      => $post->contact_name,
			'to_email'     => $post->email,
			'to_phone'     => $post->phone,
			'subject'      => $post->title,
			'message'      => $request->input('message')
				. '<br><br>'
				. t('Related to the ad')
				. ': <a href="' . lurl($post->uri) . '">' . t('Click here to see') . '</a>',
			'filename'     => $resume->filename,
		]);
		$message->save();
		
		// Save the Resume file (for Guest Users)
		if (!Auth::check()) {
			if ($request->hasFile('resume.filename')) {
				$message->filename = $request->file('resume.filename');
				$message->save();
			}
		}
		
		// Send a message to publisher
		try {
			$post->notify(new EmployerContacted($post, $message));
			
			$msg = t("Your message has sent successfully to :contact_name.", ['contact_name' => $post->contact_name]);
			flash($msg)->success();
		} catch (\Exception $e) {
			flash($e->getMessage())->error();
		}
		
		return redirect(config('app.locale') . '/' . $post->uri);
	}
	
	/**
	 * Get similar Posts (Posts in the same Category)
	 *
	 * @param $cat
	 * @param int $currentPostId
	 * @return array|null|\stdClass
	 */
	private function getCategorySimilarPosts($cat, $currentPostId = 0)
	{
		$limit = 20;
		$featured = null;
		
		// Get the sub-categories of the current ad parent's category
		$similarCatIds = [];
		if (!empty($cat)) {
			if ($cat->tid == $cat->parent_id) {
				$similarCatIds[] = $cat->tid;
			} else {
				if (!empty($cat->parent_id)) {
					$similarCatIds = Category::trans()->where('parent_id', $cat->parent_id)->get()->keyBy('id')->keys()->toArray();
					$similarCatIds[] = (int)$cat->parent_id;
				} else {
					$similarCatIds[] = (int)$cat->tid;
				}
			}
		}
		
		// Get ads from same category
		$posts = [];
		if (!empty($similarCatIds)) {
			if (count($similarCatIds) == 1) {
				$similarPostSql = 'AND a.category_id=' . ((isset($similarCatIds[0])) ? (int)$similarCatIds[0] : 0) . ' ';
			} else {
				$similarPostSql = 'AND a.category_id IN (' . implode(',', $similarCatIds) . ') ';
			}
			$reviewedPostSql = '';
			if (config('settings.single.posts_review_activation')) {
				$reviewedPostSql = ' AND a.reviewed = 1';
			}
			$sql = 'SELECT a.* ' . '
				FROM ' . table('posts') . ' as a
				WHERE a.country_code = :countryCode ' . $similarPostSql . '
					AND (a.verified_email=1 AND a.verified_phone=1)
					AND a.archived!=1 
					AND a.deleted_at IS NULL ' . $reviewedPostSql . '
					AND a.id != :currentPostId
				ORDER BY a.id DESC
				LIMIT 0,' . (int)$limit;
			$bindings = [
				'countryCode'   => config('country.code'),
				'currentPostId' => $currentPostId,
			];
			
			$cacheId = 'profiles.similar.category.' . $cat->tid . '.profile.' . $currentPostId;
			$profiles = Cache::remember($cacheId, $this->cacheExpiration, function () use ($sql, $bindings) {
				try {
					$profiles = DB::select(DB::raw($sql), $bindings);
				} catch (\Exception $e) {
					return [];
				}
				
				return $profiles;
			});
		}
		
		if (count($posts) > 0) {
			// Append the Posts 'uri' attribute
			$posts = collect($posts)->map(function ($post) {
				$post->uri = trans('routes.v-post', ['slug' => slugify($post->title), 'id' => $post->id]);
				
				return $post;
			})->toArray();
			
			// Randomize the Posts
			$posts = collect($posts)->shuffle()->toArray();
			
			// Featured Area Data
			$featured = [
				'title' => t('Similar Jobs'),
				'link'  => qsurl(config('app.locale') . '/' . trans('routes.v-search', ['countryCode' => config('country.icode')]), array_merge(Request::except('c'), ['c' => $cat->tid])),
				'posts' => $posts,
			];
			$featured = Arr::toObject($featured);
		}
		
		return $featured;
	}
	
	/**
	 * Get Posts in the same Location
	 *
	 * @param $city
	 * @param int $currentPostId
	 * @return array|null|\stdClass
	 */
	private function getLocationSimilarPosts($city, $currentPostId = 0)
	{
		$distance = 50; // km
		$limit = 20;
		$carousel = null;
		
		if (!empty($city)) {
			// Get ads from same location (with radius)
			$reviewedPostSql = '';
			if (config('settings.single.posts_review_activation')) {
				$reviewedPostSql = ' AND a.reviewed = 1';
			}
			$sql = 'SELECT a.*, 3959 * acos(cos(radians(' . $city->latitude . ')) * cos(radians(a.lat))'
				. '* cos(radians(a.lon) - radians(' . $city->longitude . '))'
				. '+ sin(radians(' . $city->latitude . ')) * sin(radians(a.lat))) as distance
				FROM ' . table('posts') . ' as a
				INNER JOIN ' . table('categories') . ' as c ON c.id=a.category_id AND c.active=1
				WHERE a.country_code = :countryCode
					AND (a.verified_email=1 AND a.verified_phone=1)
					AND a.archived!=1 
					AND a.deleted_at IS NULL ' . $reviewedPostSql . '
					AND a.id != :currentPostId
				HAVING distance <= ' . $distance . ' 
				ORDER BY distance ASC, a.id DESC
				LIMIT 0,' . (int)$limit;
			$bindings = [
				'countryCode'   => config('country.code'),
				'currentPostId' => $currentPostId,
			];
			
			$cacheId = 'posts.similar.city.' . $city->id . '.post.' . $currentPostId;
			$posts = Cache::remember($cacheId, $this->cacheExpiration, function () use ($sql, $bindings) {
				try {
					$posts = DB::select(DB::raw($sql), $bindings);
				} catch (\Exception $e) {
					return [];
				}
				
				return $posts;
			});
			
			if (count($posts) > 0) {
				// Append the Posts 'uri' attribute
				$posts = collect($posts)->map(function ($post) {
					$post->uri = trans('routes.v-post', ['slug' => slugify($post->title), 'id' => $post->id]);
					
					return $post;
				})->toArray();
				
				// Randomize the Posts
				$posts = collect($posts)->shuffle()->toArray();
				
				// Featured Area Data
				$carousel = [
					'title' => t('More jobs at :distance :unit around :city', [
						'distance' => $distance,
						'unit'     => unitOfLength(config('country.code')),
						'city'     => $city->name,
					]),
					'link'  => qsurl(config('app.locale') . '/' . trans('routes.v-search', ['countryCode' => config('country.icode')]), array_merge(Request::except(['l', 'location']), ['l' => $city->id])),
					'posts' => $posts,
				];
				$carousel = Arr::toObject($carousel);
			}
		}
		
		return $carousel;
	}
}

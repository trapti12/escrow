<?php
    

namespace App\Http\Controllers\Profile\Traits;

use App\Helpers\Arr;
use App\Helpers\Ip;
use App\Http\Requests\EmployeeProfileRequest;
use App\Models\EmployeeProfile;
use App\Models\EmployeeProfileEducation;
use App\Models\EmployeeProfileEmployment;
use App\Models\EmployeeProfileExtra;
use App\Models\City;
use App\Models\Scopes\VerifiedScope;
use App\Models\Scopes\ReviewedScope;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Torann\LaravelMetaTags\Facades\MetaTag;

trait EditTrait
{
	/**
	 * Show the form the create a new ad profile.
	 *
	 * @param $profileIdOrToken
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getUpdateBasicForm($profileIdOrToken)
	{
		$data = [];
		
		// Get EmployeeProfile
		if (getSegment(2) == 'create') {
			if (!Session::has('tmpPostId')) {
				return redirect('profile/create');
			}
			$profile = EmployeeProfile::withoutGlobalScopes([VerifiedScope::class, ReviewedScope::class])->where('id', session('tmpPostId'))->where('tmp_token', $profileIdOrToken)->first();
		} else {
			$profile = EmployeeProfile::withoutGlobalScopes([VerifiedScope::class, ReviewedScope::class])->where('user_id', auth()->user()->id)->where('id', $profileIdOrToken)->first();
		}
		
		if (empty($profile)) {
			abort(404);
		}
		view()->share('profile', $profile);		
		
		
		// Get the Profile's Administrative Division
		if (config('country.admin_field_active') == 1 && in_array(config('country.admin_type'), ['1', '2'])) {
			// Get the Profile's City
			$city = City::find($post->city_id);
			if (!empty($city)) {
				$adminType = config('country.admin_type');
				$adminModel = '\App\Models\SubAdmin' . $adminType;
				
				// Get the City's Administrative Division
				$admin = $adminModel::where('code', $city->{'subadmin' . $adminType . '_code'})->first();
				if (!empty($admin)) {
					view()->share('admin', $admin);
				}
			}
		}
		
		// Meta Tags
		MetaTag::set('title', 'Update My Profile');
		MetaTag::set('description', 'Update My Profile');
		
		return view('profile.employee.edit', $data);
	}
	
	/**
	 * Update the Profile
	 *
	 * @param $profileIdOrToken
	 * @param EmployeeProfileRequest $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function postUpdateBasicForm($profileIdOrToken, EmployeeProfileRequest $request)
	{
		// Get Profile
		if (getSegment(2) == 'create') {
			if (!Session::has('tmpProfileId')) {
				return redirect('posts/create');
			}
			$profile = EmployeeProfile::withoutGlobalScopes([VerifiedScope::class, ReviewedScope::class])->where('id', session('tmpProfileId'))->where('tmp_token', $profileIdOrToken)->first();
		} else {
			$profile = EmployeeProfile::withoutGlobalScopes([VerifiedScope::class, ReviewedScope::class])->where('user_id', auth()->user()->id)->where('id', $profileIdOrToken)->first();
		}
		
		if (empty($profile)) {
			abort(404);
		}
		
		// Get the Profile's City
		$city = City::find($request->input('city', 0));
		if (empty($city)) {
			flash(t("Posting Ads was disabled for this time. Please try later. Thank you."))->error();
			
			return back()->withInput($request->except('company.logo'));
		}
		
		// Conditions to Verify User's Email or Phone
		$emailVerificationRequired = config('settings.mail.email_verification') == 1 && $request->filled('email') && $request->input('email') != $profile->email;
		$phoneVerificationRequired = config('settings.sms.phone_verification') == 1 && $request->filled('phone') && $request->input('phone') != $profile->phone;		
		
		
		// Update the Profile
		/*
		 * Allow admin users to approve the changes,
		 * If the ads approbation option is enable,
		 * And if important data have been changed.
		 */
		if (config('settings.single.posts_review_activation')) {
			if (
				md5($profile->title) != md5($request->input('title')) ||
				md5($profile->company_description) != md5((isset($company->description)) ? $company->description : null) ||
				md5($profile->description) != md5($request->input('description')) ||
				md5($profile->application_url) != md5($request->input('application_url'))
			) {
				$profile->reviewed = 0;
			}
		}

		/*// $post->country_code = $request->input('country_code');
		$post->category_id = $request->input('category');
		$post->post_type_id = $request->input('post_type');
		$post->company_id = (isset($company->id)) ? $company->id : 0;
		$post->company_name = (isset($company->name)) ? $company->name : null;
		$post->logo = (isset($company->logo)) ? $company->logo : null;
		$post->company_description = (isset($company->description)) ? $company->description : null;
		$post->title = $request->input('title');
		$post->description = $request->input('description');
		$post->tags = $request->input('tags');
		$post->salary_min = $request->input('salary_min');
		$post->salary_max = $request->input('salary_max');
		$post->salary_type_id = $request->input('salary_type');
		$post->negotiable = $request->input('negotiable');
		$post->start_date = $request->input('start_date');
		$post->contact_name = $request->input('contact_name');
		$post->email = $request->input('email');
		$post->phone = $request->input('phone');
		$post->phone_hidden = $request->input('phone_hidden');
		$post->city_id = $request->input('city');
		$post->lat = $city->latitude;
		$post->lon = $city->longitude;
		$post->application_url = $request->input('application_url');
		$post->ip_addr = Ip::get();*/
		
		// Email verification key generation
		if ($emailVerificationRequired) {
			$profile->email_token = md5(microtime() . mt_rand());
			$profile->verified_email = 0;
		}
		
		// Phone verification key generation
		if ($phoneVerificationRequired) {
			$profile->phone_token = mt_rand(100000, 999999);
			$profile->verified_phone = 0;
		}
		
		// Save Profile
		$profile->save();
		
		// Get Next URL
		/*$creationPath = (getSegment(2) == 'create') ? 'create/' : '';
		if (
			isset($this->data['countPackages']) &&
			isset($this->data['countPaymentMethods']) &&
			$this->data['countPackages'] > 0 &&
			$this->data['countPaymentMethods'] > 0
		) {
			flash(t("Your ad has been updated."))->success();
			$nextStepUrl = config('app.locale') . '/posts/' . $creationPath . $postIdOrToken . '/packages';
		} else {
			if (getSegment(1) == 'create') {
				$request->session()->flash('message', t('Your ad has been created.'));
				$nextStepUrl = config('app.locale') . '/posts/create/' . $profileIdOrToken . '/finish';
			} else {
				flash(t("Your ad has been updated."))->success();
				$nextStepUrl = config('app.locale') . '/' . $profile->uri;
			}
		}*/
		
		// Send Email Verification message
		if ($emailVerificationRequired) {
			$this->sendVerificationEmail($profile);
			$this->showReSendVerificationEmailLink($profile, 'profile');
		}
		
		// Send Phone Verification message
		if ($phoneVerificationRequired) {
			// Save the Next URL before verification
			session(['itemNextUrl' => $nextStepUrl]);
			
			$this->sendVerificationSms($profile);
			$this->showReSendVerificationSmsLink($profile, 'profile');
			
			// Go to Phone Number verification
			$nextStepUrl = config('app.locale') . '/verify/profile/phone/';
		}
		
		// Redirection
		return redirect($nextStepUrl);
	}

	/*************** Education Edit Forms ********************/
	/**
	 * Show the form the create a new ad profile.
	 *
	 * @param $profileIdOrToken
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getUpdateEducationForm($profileIdOrToken)
	{
		$data = [];
		

		// Meta Tags
		MetaTag::set('title', 'Update My Profile');
		MetaTag::set('description', 'Update My Profile');
		
		return view('profile.employee.edit', $data);
	}
	/**
	 * Update the Profile
	 *
	 * @param $profileIdOrToken
	 * @param EmployeeProfileRequest $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function postUpdateEducationForm($profileIdOrToken, EmployeeProfileRequest $request)
	{

		// Redirection
		return redirect($nextStepUrl);
	}


	/*************** Empployment Edit Forms ********************/
	/**
	 * Show the form the create a new ad profile.
	 *
	 * @param $profileIdOrToken
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getUpdateEmploymentForm($profileIdOrToken)
	{
		$data = [];
		

		// Meta Tags
		MetaTag::set('title', 'Update My Profile');
		MetaTag::set('description', 'Update My Profile');
		
		return view('profile.employee.edit', $data);
	}
	/**
	 * Update the Profile
	 *
	 * @param $profileIdOrToken
	 * @param EmployeeProfileRequest $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function postUpdateEmploymentForm($profileIdOrToken, EmployeeProfileRequest $request)
	{

		// Redirection
		return redirect($nextStepUrl);
	}

	
	/*************** Extra Edit Forms ********************/
	/**
	 * Show the form the create a new ad profile.
	 *
	 * @param $profileIdOrToken
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getUpdateExtraForm($profileIdOrToken)
	{
		$data = [];
		

		// Meta Tags
		MetaTag::set('title', 'Update My Profile');
		MetaTag::set('description', 'Update My Profile');
		
		return view('profile.employee.edit', $data);
	}
	/**
	 * Update the Profile
	 *
	 * @param $profileIdOrToken
	 * @param EmployeeProfileRequest $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function postUpdateExtraForm($profileIdOrToken, EmployeeProfileRequest $request)
	{

		// Redirection
		return redirect($nextStepUrl);
	}
}

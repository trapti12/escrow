<?php
    

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;

class EmployeeProfileRequest extends Request
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$cat = null;
		
		$input = Request::all();
		//echo "<pre>"; print_R($input);die;
		$rules = [
			'name'   =>'required',
			'email'  =>'required',
			'phone'  =>'required',
			/* 'category'     => 'required',
			'resume_heading'        => 'required|mb_between:2,150|whitelist_word_title',
			'current_designation'  => 'required|mb_between:2,100|whitelist_word',
			'total_experience'  => 'required',
			'current_company' => 'required|mb_between:2,200',
			'annual_salary'        => 'max:100|whitelist_email|whitelist_domain',
			//'current_location'        => 'max:20',
			'highest_degree'         => 'required',
			//'preferred_location'         => 'required',
			'functional_area'         => 'required',
			'role'         => 'required',
			'marital_status'         => 'required',
			'city'         => 'required',
			'date_of_birth'         => 'required',
			'profile_summary'         => 'required',
			'employment_details'         => 'required',
			'profile_summary'         => 'required',
			'job_type'        => 'required',
			'expected_annual_salary'   => 'required', */
		];

		if ($this->hasFile('resume.filename')) {
            $rules['resume.filename'] = 'required|mimes:' . getUploadFileTypes('file') . '|max:' . (int)config('settings.upload.max_file_size', 1000);
        }

		return $rules;
	}
	
	/**
	 * @return array
	 */
	public function messages()
	{
		$messages = [];
		
		return $messages;
	}
}

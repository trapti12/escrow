<?php

namespace App\Models;

use App\Models\Scopes\FromActivatedCategoryScope;
use App\Models\Scopes\VerifiedScope;
use App\Models\Scopes\ReviewedScope;
use App\Models\Traits\CountryTrait;
use App\Observer\PostObserver;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Jenssegers\Date\Date;
use Larapen\Admin\app\Models\Crud;
use Carbon\Carbon;
use DB;

class EmployeeProfile extends BaseModel 
{
    use Crud, CountryTrait, Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ee_profile';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    protected $appends = ['uri', 'created_at_ta'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     
    protected $fillable = [
                'country_code',
                'user_id',
                'profile_name',
                'name',
                'email',
                'phone',
                'verified_email',
                'verified_phone',
                'email_token',
                'phone_token',
                'resume_headline',
                'total_experience',
                'ee_profile_type',
                'industry_id',
                'functional_area_id',
                'role_id',        		
                'current_location',
                'prefered_location',
                'lon',
                'lat', 
                'skills',
                'current_salary',
                'notice_period',
                'notice_period_status',
                'expected_salary',                
                'it_skills',
                'profile_summery',
                'photo',
                'resume',
                'visits',
                'downloads',
                'archived',
                'created_at',
                'updated_at',
                'deleted_at'
			];


    /**
     * The attributes that should be hidden for arrays
     *
     * @var array
     */
    // protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    protected static function boot()
    {
        parent::boot();
		EmployeeProfile::observe(EmployeeProfileObserver::class);
    }

    

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function employeeProfileEducation()
    {
        return $this->hasMany(EmployeeProfileEducation::class, 'ee_profile_id');
    } 

    public function employeeProfileEmployment()
    {
        return $this->hasMany(EmployeeProfileEmployment::class, 'ee_profile_id');
    }

    public function employeeProfileExtra()
    {
        return $this->hasMany(EmployeeProfileExtra::class, 'ee_profile_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }  

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'post_id');
    }

    public function onePayment()
    {
        return $this->hasOne(Payment::class, 'post_id');
    }

    public function pictures()
    {
        return $this->hasMany(Picture::class, 'post_id')->orderBy('position')->orderBy('id');
    }

    public function savedByUsers()
    {
        return $this->hasMany(SavedPost::class, 'post_id');
    }
    
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopeArchived($builder)
    {
        return $builder->where('archived', 1);
    }
    
    public function scopeUnarchived($builder)
    {
        return $builder->where('archived', 0);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    
    public static function getPhoto($value)
    {
        // OLD PATH
        $value = str_replace('uploads/employee/photo/', '', $value);
        $value = str_replace('employee/photo/', '', $value);
        $value = 'employee/photo/' . $value;
        if (Storage::exists($value) && substr($value, -1) != '/') {
            return $value;
        }

        // NEW PATH
        $value = str_replace('pictures/', '', $value);
        if (!Storage::exists($value) && substr($value, -1) != '/') {
            $value = config('larapen.core.picture.default');
        }

        return $value;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    

    public function setResumeAttribute($value)
    {
        $field_name = 'resume.filename';
        $attribute_name = 'resume';
        $disk = config('filesystems.default');
    
        // Set the right field name
        $request = \Request::instance();
        if (!$request->hasFile($field_name)) {
            $field_name = $attribute_name;
        }

        if (!isset($this->country_code) || !isset($this->id)) {
            $this->attributes[$attribute_name] = null;
            return false;
        }

        // Path
        $destination_path = 'employee/resumes/' . strtolower($this->country_code) . '/' . $this->id;

        // Upload
        $this->uploadFileToDiskCustom($value, $field_name, $attribute_name, $disk, $destination_path);
    }

    public function setPhotoAttribute($value)
    {
        $field_name = 'photo';
        $attribute_name = 'photo';
        $disk = config('filesystems.default');
    
        // Set the right field name
        $request = \Request::instance();
        if (!$request->hasFile($field_name)) {
            $field_name = $attribute_name;
        }

        if (!isset($this->country_code) || !isset($this->id)) {
            $this->attributes[$attribute_name] = null;
            return false;
        }

        // Path
        $destination_path = 'employee/photos/' . strtolower($this->country_code) . '/' . $this->id;

        // Upload
        $this->uploadFileToDiskCustom($value, $field_name, $attribute_name, $disk, $destination_path);
    }
	
	public function setTagsAttribute($value)
	{
		$this->attributes['tags'] = (!empty($value)) ? mb_strtolower($value) : $value;
	}
	
	
}

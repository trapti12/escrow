<?php

namespace App\Models;

use App\Models\Scopes\FromActivatedCategoryScope;
use App\Models\Scopes\VerifiedScope;
use App\Models\Scopes\ReviewedScope;
use App\Models\Traits\CountryTrait;
use App\Observer\PostObserver;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Jenssegers\Date\Date;
use Larapen\Admin\app\Models\Crud;
use Spatie\Feed\Feedable;
use Spatie\Feed\FeedItem;
use Carbon\Carbon;
use DB;

class EmployeeProfileEmployment extends BaseModel implements Feedable 
{
    use Crud, CountryTrait, Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ee_profile_employment';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    protected $appends = ['uri', 'created_at_ta'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //working_status = {'Previous','Current'}; 
    protected $fillable = [
                'ee_profile_id',
                'designation',
                'company',
                'from_date',
                'to_date',
                'description',
                'working_status',
                'created_at',
                'updated_at',
                'deleted_at'
			];


    /**
     * The attributes that should be hidden for arrays
     *
     * @var array
     */
    // protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    protected static function boot()
    {
        parent::boot();
		Post::observe(PostObserver::class);
    }

    public static function getFeedItems()
    {
        $postsPerPage = (int)config('settings.listing.items_per_page', 50);
        
        if (request()->has('d')) {
            $posts = Post::where('country_code', request()->input('d'))
                         ->take($postsPerPage)
                         ->orderByDesc('id')
                         ->get();
        } else {
            $posts = Post::take($postsPerPage)->orderByDesc('id')->get();
        }
        
        return $posts;
    }
    
    public function toFeedItem()
    {
        $title = $this->title;
        $title .= (isset($this->city) && !empty($this->city)) ? ' - ' . $this->city->name : '';
        $title .= (isset($this->country) && !empty($this->country)) ? ', ' . $this->country->name : '';
        // $summary = str_limit(str_strip(strip_tags($this->description)), 5000);
        $summary = transformDescription($this->description);
        $link = config('app.locale') . '/' . $this->uri;
        
        return FeedItem::create()
                       ->id($link)
                       ->title($title)
                       ->summary($summary)
                       ->updated($this->updated_at)
                       ->link($link)
                       ->author($this->contact_name);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function employeeProfile()
    {
        return $this->belongsTo('App\Models\EmployeeProfile', 'ee_profile_id');
    }
    

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */    
    
    public function scopeArchived($builder)
    {
        return $builder->where('archived', 1);
    }
    

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
    public function getCreatedAtAttribute($value)
    {
        $value = Date::parse($value);
        if (config('timezone.id')) {
            $value->timezone(config('timezone.id'));
        }
        //echo $value->format('l d F Y H:i:s').'<hr>'; exit();
        //echo $value->formatLocalized('%A %d %B %Y %H:%M').'<hr>'; exit(); // Multi-language

        return $value;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */    
    
    public function setTagsAttribute($value)
    {
        $this->attributes['tags'] = (!empty($value)) ? mb_strtolower($value) : $value;
    }
    
    public function setApplicationUrlAttribute($value)
    {
        $this->attributes['application_url'] = (!empty($value)) ? strtolower($value) : $value;
    }
}

<?php

namespace App\Events;

use App\Events\Event;
use App\Models\EmployeeProfile;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EmployeeProfileVisited extends Event
{
    use SerializesModels;
    
    public $profile;
    
    /**
     * Create a new event instance.
     *
     * @param EmployeeProfile $profile
     */
    public function __construct(EmployeeProfile $profile)
    {
        $this->profile = $profile;
                
    }
    
    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}


@extends('layouts.master')

<?php
// Phone
$phone = TextToImage::make($profile->phone, IMAGETYPE_PNG, ['backgroundColor' => 'rgba(0,0,0,0.0)', 'color' => '#FFFFFF']);
$phoneLink = 'tel:' . $profile->phone;
$phoneLinkAttr = '';
if (!auth()->check()) {
	if (config('settings.single.guests_can_apply_jobs') != '1') {
		$phone = t('Click to see');
		$phoneLink = '#quickLogin';
		$phoneLinkAttr = 'data-toggle="modal"';
	}
}

// Contact Recruiter URL
$applyJobURL = '#applyJob';
$applyLinkAttr = 'data-toggle="modal"';
if (!empty($profile->application_url)) {
	$applyJobURL = $profile->application_url;
	$applyLinkAttr = '';
}
if (!auth()->check()) {
	if (config('settings.single.guests_can_apply_jobs') != '1') {
		$applyJobURL = '#quickLogin';
		$applyLinkAttr = 'data-toggle="modal"';
	}
}
?>

@section('content')
	{!! csrf_field() !!}
	<input type="hidden" id="post_id" value="{{ $profile->id }}">
	
	@if (Session::has('flash_notification'))
		@include('common.spacer')
		<?php $paddingTopExists = true; ?>
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					@include('flash::message')
				</div>
			</div>
		</div>
		<?php Session::forget('flash_notification.message'); ?>
	@endif
	
	<div class="main-container">
		
		<?php if (\App\Models\Advertising::where('slug', 'top')->count() > 0): ?>
			@include('layouts.inc.advertising.top', ['paddingTopExists' => (isset($paddingTopExists)) ? $paddingTopExists : false])
		<?php
			$paddingTopExists = false;
		endif;
		?>
		@include('common.spacer')

		<div class="container">
			<ol class="breadcrumb pull-left">
				<li><a href="{{ lurl('/') }}"><i class="icon-home fa"></i></a></li>
				<li><a href="{{ lurl('/') }}">{{ config('country.name') }}</a></li>				
				<li class="active">{{ str_limit($profile->profile_name, 70) }}</li>
			</ol>
			<div class="pull-right backtolist">
				<a href="{{ URL::previous() }}">
					<i class="fa fa-angle-double-left"></i> {{ t('Back to Results') }}
				</a>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-sm-9 page-content col-thin-right">
					<div class="inner inner-box ads-details-wrapper">
						<h2 class="enable-long-words">
							<strong>
                                <a href="{{ lurl($profile->uri) }}" title="{{ mb_ucfirst($profile->profile_name) }}">
                                    {{ mb_ucfirst($profile->profile_name) }}
                                </a>
                            </strong>
							<small class="label label-default adlistingtype"></small>
                            
						</h2>
						<span class="info-row">
							<span class="date"><i class=" icon-clock"> </i> {{ $profile->created_at }} </span> -&nbsp;
							<span class="category">{{ $profile->industry_id }}</span> -&nbsp;
							<span class="item-location"><i class="fa fa-map-marker"></i> {{ $profile->current_location }} </span> -&nbsp;
							<span class="category"><i class="icon-eye-3"></i> {{ $profile->visits }} {{ trans_choice('global.count_views', getPlural($profile->visits)) }}</span>
						</span>

						<div class="ads-details">
							<div class="row" style="padding-bottom: 20px;">
								<div class="ads-details-info jobs-details-info col-md-8 enable-long-words from-wysiwyg">
									<h5 class="list-title"><strong>Profile Details</strong></h5>
									
									<!-- Description -->
                                    <div>
										{!! transformDescription($profile->description) !!}
                                    </div>
									
									
									<!-- Tags -->
									@if (!empty($profile->tags))
										<?php $tags = explode(',', $profile->tags); ?>
										@if (!empty($tags))
											<div style="clear: both;"></div>
											<div class="tags">
												<h5 class="list-title"><strong>{{ t('Tags') }}</strong></h5>
												@foreach($tags as $iTag)
													<?php $attr = ['countryCode' => config('country.icode'), 'tag' => $iTag]; ?>
													<a href="{{ lurl(trans('routes.v-search-tag', $attr), $attr) }}">
														{{ $iTag }}
													</a>
												@endforeach
											</div>
										@endif
									@endif
								</div>
								<div class="col-md-4">
									<aside class="panel panel-body panel-details job-summery">
										<ul>											
											<li>
												<p class="no-margin">
													<strong>{{ t('Company') }}:</strong>&nbsp;
													
												</p>
											</li>
											<li>
												<p class="no-margin">
													<strong>{{ t('Salary') }}:</strong>&nbsp;
													
												</p>
											</li>
											<li>												
												
											</li>
											<li>
												<p class="no-margin">
													<strong>{{ t('Location') }}:</strong>&nbsp;
													<?php $attr = [
															'countryCode' => config('country.icode'),
															'city'        => slugify($profile->current_location),
															'id'          => $profile->current_location
														]; ?>
													<a href="{!! lurl(trans('routes.v-search-city', $attr), $attr) !!}">
														{{ $profile->current_location }}
													</a>
												</p>
											</li>
										</ul>
									</aside>
									<div class="ads-action">
										<ul class="list-border">
											
											<li id="{{ $profile->id }}">
												<a class="make-favorite">
												@if (auth()->check())
													@if (\App\Models\SavedPost::where('user_id', auth()->user()->id)->where('post_id', $profile->id)->count() > 0)
														<i class="fa fa-heart"></i> Save Profile
													@else
														<i class="fa fa-heart-o"></i> Save Profile
													@endif
												@else
													<i class="fa fa-heart-o"></i> Save Profile
												@endif
                                                </a>
											</li>
											<li>
												<a href="{{ lurl('posts/' . $profile->id . '/report') }}">
													<i class="fa icon-info-circled-alt"></i> {{ t('Report abuse') }}
												</a>
											</li>
										</ul>
									</div>
								</div>

								<br>&nbsp;<br>
							</div>
							<div class="content-footer text-left">
								@if (auth()->check())
									@if (auth()->user()->id == $profile->user_id)
										<a class="btn btn-default" href="{{ lurl('posts/'.$profile->id.'/edit') }}">
											<i class="fa fa-pencil-square-o"></i> {{ t('Edit') }}
										</a>
									@else
										@if ($profile->email != '' and in_array(auth()->user()->user_type_id, [1, 3]))
											<a class="btn btn-default" {!! $applyLinkAttr !!} href="{{ $applyJobURL }}">
												<i class="icon-mail-2"></i> {{ t('Apply Online') }}
											</a>
										@endif
									@endif
								@else
									@if ($profile->email != '')
										<a class="btn btn-default" {!! $applyLinkAttr !!} href="{{ $applyJobURL }}">
											<i class="icon-mail-2"></i> {{ t('Apply Online') }}
										</a>
									@endif
								@endif
								@if ($profile->phone_hidden != 1 and !empty($profile->phone))
									<a href="{{ $phoneLink }}" {!! $phoneLinkAttr !!} class="btn btn-success showphone">
										<i class="icon-phone-1"></i>
										{!! $phone !!}{{-- t('View phone') --}}
									</a>
								@endif
								&nbsp;
							</div>
						</div>
					</div>
					<!--/.ads-details-wrapper-->
				</div>
				<!--/.page-content-->

				<div class="col-sm-3 page-sidebar-right">
					<aside>
						<div class="panel sidebar-panel panel-contact-seller">
							<div class="panel-heading">Basic Information</div>
							<div class="panel-content user-info">
								<div class="panel-body text-center">
									<div class="seller-info">
										<div class="company-logo-thumb mb20">
											@if (isset($profile->photo) and !empty($profile->photo))
												
												<a href="{{ lurl(trans('routes.v-search-company', $attr), $attr) }}">
													<img alt="Logo {{ $profile->profile_name }}" class="img-responsive" src="{{ resize($profile->photo, 'big') }}">
												</a>
											@else
												<img alt="Logo {{ $profile->profile_name }}" class="img-responsive" src="{{ resize($profile->photo, 'big') }}">
											@endif
										</div>
										
										<p>
											{{ t('Location') }}:&nbsp;
											<strong>
												<?php $attr = [
														'countryCode' => config('country.icode'),
														'city'        => slugify($profile->current_location),
														'id'          => $profile->current_location
													]; ?>
												<a href="{!! lurl(trans('routes.v-search-city', $attr), $attr) !!}">
													{{ $profile->current_location }}
												</a>
											</strong>
										</p>
										
									</div>
									<div class="user-ads-action">
										@if (auth()->check())
											@if (auth()->user()->id == $profile->user_id)
												<a href="{{ lurl('posts/'.$profile->id.'/edit') }}" data-toggle="modal" class="btn btn-default btn-block">
													<i class="fa fa-pencil-square-o"></i> {{ t('Edit') }}
												</a>
											@else
												@if ($profile->email != '' and in_array(auth()->user()->user_type_id, [1, 3]))
													<a href="{{ $applyJobURL }}" {!! $applyLinkAttr !!} class="btn btn-default btn-block">
														<i class="icon-mail-2"></i> {{ t('Apply Online') }}
													</a>
												@endif
											@endif
										@else
											@if ($profile->email != '')
												<a href="{{ $applyJobURL }}" {!! $applyLinkAttr !!} class="btn btn-default btn-block">
													<i class="icon-mail-2"></i> {{ t('Apply Online') }}
												</a>
											@endif
										@endif
										@if ($profile->phone_hidden != 1 and !empty($profile->phone))
											<a href="{{ $phoneLink }}" {!! $phoneLinkAttr !!} class="btn btn-success btn-block showphone">
												<i class="icon-phone-1"></i>
                                                {!! $phone !!}{{-- t('View phone') --}}
											</a>
										@endif
									</div>
								</div>
							</div>
						</div>
						
						@if (config('settings.single.show_post_on_googlemap'))
							<div class="panel sidebar-panel">
								<div class="panel-heading">{{ t('Location\'s Map') }}</div>
								<div class="panel-content">
									<div class="panel-body text-left" style="padding: 0;">
										<div class="ads-googlemaps">
											<iframe id="googleMaps" width="100%" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src=""></iframe>
										</div>
									</div>
								</div>
							</div>
						@endif
						
						@if (isVerifiedPost($profile))
							@include('layouts.inc.social.horizontal')
						@endif

						<div class="panel sidebar-panel">
							<div class="panel-heading">{{ t('Tips for candidates') }}</div>
							<div class="panel-content">
								<div class="panel-body text-left">
									<ul class="list-check">
										<li> {{ t('Check if the offer matches your profile') }} </li>
                                        <li> {{ t('Check the start date') }} </li>
										<li> {{ t('Meet the employer in a professional location') }} </li>
									</ul>
                                    <?php $tipsLinkAttributes = getUrlPageByType('tips'); ?>
									@if (!str_contains($tipsLinkAttributes, 'href="#"') and !str_contains($tipsLinkAttributes, 'href=""'))
									<p>
										<a class="pull-right" {!! $tipsLinkAttributes !!}>
											{{ t('Know more') }}
											<i class="fa fa-angle-double-right"></i>
										</a>
									</p>
                                    @endif
								</div>
							</div>
						</div>
					</aside>
				</div>
			</div>

		</div>
		
		@include('home.inc.featured', ['firstSection' => false])
		@include('layouts.inc.advertising.bottom', ['firstSection' => false])
		@if (isVerifiedPost($profile))
			@include('layouts.inc.tools.facebook-comments', ['firstSection' => false])
		@endif
		
	</div>
@endsection

@section('modal_message')
	@if (auth()->check() or config('settings.single.guests_can_apply_jobs')=='1')
		@include('profile.employee.inc.compose-message')
	@endif
@endsection

@section('after_styles')
@endsection

@section('after_scripts')
    @if (config('services.googlemaps.key'))
        <script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.googlemaps.key') }}" type="text/javascript"></script>
    @endif
	
	<script>
		/* Favorites Translation */
        var lang = {
            labelSavePostSave: "{!! t('Save Job') !!}",
            labelSavePostRemove: "{{ t('Saved Job') }}",
            loginToSavePost: "{!! t('Please log in to save the Ads.') !!}",
            loginToSaveSearch: "{!! t('Please log in to save your search.') !!}",
            confirmationSavePost: "{!! t('Post saved in favorites successfully !') !!}",
            confirmationRemoveSavePost: "{!! t('Post deleted from favorites successfully !') !!}",
            confirmationSaveSearch: "{!! t('Search saved successfully !') !!}",
            confirmationRemoveSaveSearch: "{!! t('Search deleted successfully !') !!}"
        };

		$(document).ready(function () {
			@if (config('settings.single.show_post_on_googlemap'))
				/* Google Maps */
				getGoogleMaps(
				'{{ config('services.googlemaps.key') }}',
				'{{ (isset($profile->city) and !empty($profile->city)) ? addslashes($profile->city->name) . ',' . config('country.name') : config('country.name') }}',
				'{{ config('app.locale') }}'
				);
			@endif
		})
	</script>
@endsection
  
@extends('layouts.master')

@section('wizard')
	@include('profile.inc.wizard')
@endsection

@section('content')
	@include('common.spacer')
	<div class="main-container">
		<div class="container">
			<div class="row">
				
				@include('profile.inc.notification')

				<div class="col-md-9 page-content">
					<div class="inner-box category-content">
						<h2 class="title-2"><strong> <i class="icon-docs"></i> Add Your Employee Profile</strong></h2>
						<div class="row">
							<div class="col-sm-12">
								<form class="form-horizontal" id="profileForm" method="POST" action="{{ url()->current() }}" enctype="multipart/form-data">
									{!! csrf_field() !!}
									
									<fieldset>
										
										<div class="content-subheading" style="margin-top: 0;">
											<i class="icon-town-hall fa"></i>
											<strong>Basic Info</strong>
										</div>
										<div class="form-group required <?php echo (isset($errors) and $errors->has('first_name')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label"><sup>*</sup>First Name</label>
											<div class="col-md-8">
												<input type="text" name="first_name" class="form-control input-md">
											</div>
										</div>

										<div class="form-group required <?php echo (isset($errors) and $errors->has('last_name')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label"><sup>*</sup>Last Name</label>
											<div class="col-md-8">
												<input type="text" name="last_name" class="form-control input-md">
											</div>
										</div>

										<div class="form-group required <?php echo (isset($errors) and $errors->has('resume_heading')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label"><sup>*</sup>Resume Heading</label>
											<div class="col-md-8">
												<input type="text" name="resume_heading" class="form-control input-md">
											</div>
										</div>

										<div class="form-group required <?php echo (isset($errors) and $errors->has('profile_summary')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label"><sup>*</sup> Profile Summary</label>
											<div class="col-md-8">
												<textarea name="profile_summary" class="form-control input-md" ></textarea>
											</div>
										</div>

										<div class="form-group required <?php echo (isset($errors) and $errors->has('category')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label">{{ t('Category') }} <sup>*</sup></label>
											<div class="col-md-8">
												<select name="category" id="parent" class="form-control selecter">
													<option value="0" data-type=""
															@if (old('parent')=='' or old('parent')==0)
																selected="selected"
															@endif
													> {{ t('Select a category') }} </option>

													@foreach ($categories as $cat)
														<option value="{{ $cat->tid }}" data-type="{{ $cat->type }}"
																@if (old('parent')==$cat->tid)
																	selected="selected"
																@endif
														> {{ $cat->name }} </option>
													@endforeach
												</select>
												<!-- <input type="hidden" name="parent_type" id="parent_type" value="{{ old('parent_type') }}"> -->
											</div>
										</div>										
									</div>
  									
								

										<div id="postTypeBloc" class="form-group required <?php echo (isset($errors) and $errors->has('job_type')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label">{{ t('Job Type') }} <sup>*</sup></label>
											<div class="col-md-8">
												<select name="job_type" id="job_type" class="form-control selecter">
													@foreach ($jobTypes as $jobType)
														<option value="{{ $jobType->tid }}"
																@if (old('post_type')==$jobType->tid)
																	selected="selected"
																@endif
														> {{ $jobType->name }} </option>
													@endforeach
												</select>
											</div>
										</div>

										<!-- POST -->
										<div class="content-subheading">
											<i class="icon-town-hall fa"></i>
											<strong>Current Information</strong>
										</div>

										<!-- city -->
										<div id="cityBox" class="form-group required <?php echo (isset($errors) and $errors->has('city')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label" for="city">{{ t('City') }} <sup>*</sup></label>
											<div class="col-md-8">
												<select id="city" name="city" class="form-control sselecter">
													<option value="0" {{ (!old('city') or old('city')==0) ? 'selected="selected"' : '' }}>
														{{ t('Select a city') }}
													</option>
												</select>
											</div>
										</div>
										
										<!-- parent -->
										<div class="form-group required <?php echo (isset($errors) and $errors->has('current_designation')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label">Current Designation<sup>*</sup></label>
											<div class="col-md-8">
												<input type="text"  name="current_designation" id="current_designation" class="form-control">
											</div>
										</div>

										<div class="form-group required <?php echo (isset($errors) and $errors->has('total_experience')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label">Total Experience<sup>*</sup></label>
											<div class="col-md-8">
												{!! Form::select('total_experience', Config::get('params.total_exp'), null, ['id' => 'total_experience', 'class' => 'form-control']) !!}
											</div>
										</div><div class="form-group required <?php echo (isset($errors) and $errors->has('current_company')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label">Current Company<sup>*</sup></label>
											<div class="col-md-8">
												<input type="text" name="current_company" id="current_company" class="form-control">
											</div>
										</div>

										<div class="form-group required <?php echo (isset($errors) and $errors->has('annual_salary')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label">Annual Salary<sup>*</sup></label>
											<div class="col-md-8">
												{!! Form::select('annual_salary', Config::get('params.salary_type'), null, ['id' => 'annual_salary', 'class' => 'form-control']) !!}
											</div>
										</div>
										<div class="form-group required <?php echo (isset($errors) and $errors->has('expected_annual_salary')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label">Expected Annual Salary<sup>*</sup></label>
											<div class="col-md-8">
												{!! Form::select('expected_annual_salary', Config::get('params.salary_type'), null, ['id' => 'expected_annual_salary', 'class' => 'form-control']) !!}
											</div>
										</div>
										<!-- <div class="form-group required <?php //echo (isset($errors) and $errors->has('current_location')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label">Current Location<sup>*</sup></label>
											<div class="col-md-8">
												<select id="current_location" name="current_location" class="form-control sselecter">
													<option value="0" {{ (!old('city') or old('city')==0) ? 'selected="selected"' : '' }}>
														{{ t('Select a location') }}
													</option>
												</select>
											</div>
										</div> -->
										<div class="form-group required <?php echo (isset($errors) and $errors->has('highest_degree')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label">Highest Degree<sup>*</sup></label>
											<div class="col-md-8">
												<input type="text" name="highest_degree" id="highest_degree" class="form-control">
											</div>
										</div>

										<!-- <div class="form-group required <?php // echo (isset($errors) and $errors->has('preferred_location')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label">Preferred Location<sup>*</sup></label>
											<div class="col-md-8">
												<select id="city" name="preferred_location" class="form-control sselecter">
													<option value="0" {{ (!old('city') or old('city')==0) ? 'selected="selected"' : '' }}>
														{{ t('Select a location') }}
													</option>
												</select>
											</div>
										</div> -->

										<div class="form-group required <?php echo (isset($errors) and $errors->has('functional_area')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label">Functional Area<sup>*</sup></label>
											<div class="col-md-8">
												<input type="functional_area" name="functional_area" class="form-control">
											</div>
										</div>

										<div class="form-group required <?php echo (isset($errors) and $errors->has('role')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label">Role<sup>*</sup></label>
											<div class="col-md-8">
												<input type="text" name="role" id="role" class="form-control">
											</div>
										</div>

										<div class="form-group required <?php echo (isset($errors) and $errors->has('marital_status')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label">Marital Status<sup>*</sup></label>
											<div class="col-md-8">
												{!! Form::select('marital_status', Config::get('params.marital_status'), null, ['id' => 'marital_status', 'class' => 'form-control'] ) !!}
											</div>
										</div>

										<div class="form-group required <?php echo (isset($errors) and $errors->has('date_of_birth')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label">Date Of Birth<sup>*</sup></label>
											<div class="col-md-8">
												<input type="date" name="date_of_birth" id="date_of_birth" class="form-control">
											</div>
										</div>

										<!-- description -->
										<div class="form-group required  <?php echo (isset($errors) and $errors->has('employment_details')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label" for="employment_details">Employment Details <sup>*</sup></label>
                                            <div class="col-md-8">
                                                
												<textarea class="form-control" id="employment_details" name="employment_details" rows="10"></textarea>
												<p class="help-block">{{ t('Describe what makes your ad unique') }}</p>
											</div>
										</div>

										
										<!-- start_date -->
										<!-- <div class="form-group <?php //echo (isset($errors) and $errors->has('start_date')) ? 'has-error' : ''; ?>">
											<label class="col-md-3 control-label" for="start_date">{{ t('Start Date') }} </label>
											<div class="col-md-8">
												<input id="start_date" name="start_date" placeholder="{{ t('Start Date') }}" class="form-control input-md"
													   type="text" value="{{ old('start_date') }}">
											</div>
										</div> -->

										<!-- country -->
										@if (empty(config('country.code')))
											<div class="form-group required <?php echo (isset($errors) and $errors->has('country')) ? 'has-error' : ''; ?>">
												<label class="col-md-3 control-label" for="country">{{ t('Your Country') }} <sup>*</sup></label>
												<div class="col-md-8">
													<select id="country" name="country" class="form-control sselecter">
														<option value="0" {{ (!old('country') or old('country')==0) ? 'selected="selected"' : '' }}> {{ t('Select a country') }} </option>
														@foreach ($countries as $item)
															<option value="{{ $item->get('code') }}" {{ (old('country', (!empty(config('ipCountry.code'))) ? config('ipCountry.code') : 0)==$item->get('code')) ? 'selected="selected"' : '' }}>{{ $item->get('name') }}</option>
														@endforeach
													</select>
												</div>
											</div>
										@else
											<input id="country" name="country" type="hidden" value="{{ config('country.code') }}">
										@endif

										<!-- Resume -->
										<div class="content-subheading" style="margin-top: 0;">
											<i class="icon-town-hall fa"></i>
											<strong>Update Resume</strong>
										</div>

										<div {!! (config('lang.direction')=='rtl') ? 'dir="rtl"' : '' !!} class="form-group required <?php echo (isset($errors) and $errors->has('resume.filename')) ? 'has-error' : ''; ?>">
											<label for="resume.filename" class="control-label col-md-3">{{ t('Resume File') }} </label>
											<div class="col-lg-8">
											<input id="resumeFilename" name="resume[filename]" type="file" class="file">
											<p class="help-block">{{ t('File types: :file_types', ['file_types' => showValidFileTypes('file')]) }}</p>
											@if (!empty($resume) and \Storage::exists($resume->filename))
												<div>
													<a class="btn btn-default" href="{{ \Storage::url($resume->filename) }}" target="_blank">
														<i class="icon-attach-2"></i> {{ t('Download current Resume') }}
													</a>
												</div>
											@endif
										</div>

										<!-- Button  -->
										<div class="form-group">
											<div class="col-md-12" style="text-align: center;">
												<button id="nextStepBtn" class="btn btn-primary btn-lg"> {{ t('Submit') }} </button>
											</div>
										</div>

										<div style="margin-bottom: 30px;"></div>

									</fieldset> 
								</form>


							</div>
						</div>
					</div>
				</div>
				<!-- /.page-content -->

				<div class="col-md-3 reg-sidebar">
					<div class="reg-sidebar-inner text-center">
						<div class="promo-text-box"><i class=" icon-picture fa fa-4x icon-color-1"></i>
							<h3><strong>Post Your Profile</strong></h3>
							<p>
								Do you have any requirement that is related to your business? Find the solutions in a few clicks at Fair Connects.								
							</p>
						</div>

						<div class="panel sidebar-panel">
							<div class="panel-heading uppercase">
								<small><strong>{{ t('How to find quickly a candidate?') }}</strong></small>
							</div>
							<div class="panel-content">
								<div class="panel-body text-left">
									<ul class="list-check">
										<li> {{ t('Use a brief title and description of the ad') }} </li>
										<li> {{ t('Make sure you post in the correct category') }}</li>
										<li> {{ t('Add a logo to your ad') }}</li>
										<li> {{ t('Put a min and max salary') }}</li>
										<li> {{ t('Check the ad before publish') }}</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('after_styles')
    @include('layouts.inc.tools.wysiwyg.css')
	<link href="{{ url('assets/plugins/bootstrap-fileinput/css/fileinput.min.css') }}" rel="stylesheet">
	@if (config('lang.direction') == 'rtl')
		<link href="{{ url('assets/plugins/bootstrap-fileinput/css/fileinput-rtl.min.css') }}" rel="stylesheet">
	@endif
	<style>
		.krajee-default.file-preview-frame:hover:not(.file-preview-error) {
			box-shadow: 0 0 5px 0 #666666;
		}
	</style>
@endsection

@section('after_scripts')
   {{-- @include('layouts.inc.tools.wysiwyg.js') --}} 
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/1.2.3/jquery.payment.min.js"></script>
	@if (file_exists(public_path() . '/assets/plugins/forms/validation/localization/messages_'.config('app.locale').'.min.js'))
		<script src="{{ url('assets/plugins/forms/validation/localization/messages_'.config('app.locale').'.min.js') }}" type="text/javascript"></script>
	@endif

	<script src="{{ url('assets/plugins/bootstrap-fileinput/js/plugins/sortable.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('assets/plugins/bootstrap-fileinput/js/fileinput.min.js') }}" type="text/javascript"></script>
	@if (file_exists(public_path() . '/assets/plugins/bootstrap-fileinput/js/locales/'.config('app.locale').'.js'))
		<script src="{{ url('assets/plugins/bootstrap-fileinput/js/locales/'.config('app.locale').'.js') }}" type="text/javascript"></script>
	@endif
	
	<script>
		/* Translation */
		var lang = {
			'select': {
				'category': "{{ t('Select a category') }}",
                'subCategory': "{{ t('Select a sub-category') }}",
				'country': "{{ t('Select a country') }}",
				'admin': "{{ t('Select a location') }}",
				'city': "{{ t('Select a city') }}"
			},
			'price': "{{ t('Price') }}",
			'salary': "{{ t('Salary') }}",
            'nextStepBtnLabel': {
                'next': "{{ t('Next') }}",
                'submit': "{{ t('Submit') }}"
            }
		};
		
		/* Company */
		//var postCompanyId = {{ old('company_id', (isset($postCompany) ? $postCompany->id : 0)) }};
		//getCompany(postCompanyId);
		
		/* Categories */
        var category = {{ old('parent', 0) }};
        var categoryType = '{{ old('parent_type') }}';
        if (categoryType=='') {
            var selectedCat = $('select[name=parent]').find('option:selected');
            categoryType = selectedCat.data('type');
        }
        var subCategory = {{ old('category', 0) }};
		
		/* Locations */
        var countryCode = '{{ old('country', config('country.code', 0)) }}';
        var adminType = '{{ config('country.admin_type', 0) }}';
        var selectedAdminCode = '{{ old('admin_code', (isset($admin) ? $admin->code : 0)) }}';
        var cityId = '{{ old('city', (isset($post) ? $post->city_id : 0)) }}';
		
		/* Packages */
        var packageIsEnabled = false;
		@if (isset($packages) and isset($paymentMethods) and $packages->count() > 0 and $paymentMethods->count() > 0)
            packageIsEnabled = true;
        @endif
	</script>
	<script>
		$(document).ready(function() {
			/* Company */
			$('#companyId').bind('click, change', function() {
				postCompanyId = $(this).val();
				getCompany(postCompanyId);
			});
			
			$('#tags').tagit({
				fieldName: 'tags',
				placeholderText: '{{ t('add a tag') }}',
				caseSensitive: true,
				allowDuplicates: false,
				allowSpaces: false,
				tagLimit: {{ (int)config('settings.single.tags_limit', 15) }},
				singleFieldDelimiter: ','
			});
		});
	</script>
	<script src="{{ url('assets/js/app/d.select.category.js') . vTime() }}"></script>
	<script src="{{ url('assets/js/app/d.select.location.js') . vTime() }}"></script>
@endsection

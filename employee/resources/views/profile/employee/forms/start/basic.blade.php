<?php
// From Basic Profile's Form

?>
<div id="basicProfileFields">
	
<div class="form-group required">
        <label class="col-md-3 control-label">Profile Title<sup>*</sup></label>
        <div class="col-md-8">
            <input type="text" name="profile_name" class="form-control input-md" value="" placeholder="Please provide title for the profile">
        </div>
    </div>
    <div class="form-group required">
        <label class="col-md-3 control-label">Name<sup>*</sup></label>
        <div class="col-md-8">
            <input type="text" name="name" class="form-control input-md" value="" placeholder="Please enter your full name">
        </div>
    </div>

    <!-- email -->
    <div class="form-group required">
        <label class="col-md-3 control-label" for="email">Contact Email<sup>*</sup></label>
        <div class="col-md-8">
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="icon-mail"></i>
                </span>
                <input id="email" name="email" class="form-control"
                        placeholder="{{ t('Email') }}" type="text"
                        value="{{ old('email', ((Auth::check() and isset($user->email)) ? $user->email : '')) }}">
            </div>
        </div>
    </div>

    <?php
        if (Auth::check()) {
            $formPhone = ($user->country_code == config('country.code')) ? $user->phone : '';
        } else {
            $formPhone = '';
        }
    ?>
    <!-- phone -->
    <div class="form-group required">
        <label class="col-md-3 control-label" for="phone">Phone Number</label>
        <div class="col-md-8">
            <div class="input-group">
                <span id="phoneCountry" class="input-group-addon">{!! getPhoneIcon(config('country.code')) !!}</span>
                <input id="phone" name="phone"
                        placeholder="Phone Number"
                        class="form-control input-md" type="text"
                        value="{{ phoneFormat(old('phone', $formPhone), old('country', config('country.code'))) }}">
            </div>            
        </div>
    </div>

    <!-- Employee Profile Type -->
    @if(!isset($profile->ee_profile_type))
        <input type="hidden" name="resume_type" value="{{$resume_type}}">
    @endif 
       

</div>
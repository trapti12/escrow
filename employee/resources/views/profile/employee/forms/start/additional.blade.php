<!-- Profile Photo -->
<div class="form-group ">
    <label  class="col-md-3 control-label">Add Photo<sup>*</sup> </label>
    <div class="col-md-8">
        <div {!! (config('lang.direction')=='rtl') ? 'dir="rtl"' : '' !!} class="file-loading mb10">
            <input id="photo" name="photo" type="file" class="file">
        </div>
        <p class="help-block">{{ t('File types: :file_types', ['file_types' => showValidFileTypes('image')]) }}</p>
    </div>
</div>

<!--Professional Background -->
<div class="form-group required">
    <label class="col-md-3 control-label" for="professional_background">Describe your professional background in one line <sup>*</sup></label>
    <div class="col-md-8">
           <textarea class="form-control" id="professional-background" name="professional_background" rows="4" cols="82"> </textarea> 
    </div>
</div> 


<!-- Industry -->
<div class="form-group required">
    <label class="col-md-3 control-label">Industry <sup>*</sup></label>
    <div class="col-md-8">
        <select id="industry" class="form-control" name="industry">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select> 
    </div>
</div>

<!-- Functional Area -->
<div class="form-group required">
    <label class="col-md-3 control-label">Functional Area <sup>*</sup></label>
    <div class="col-md-8">
        <select id="functional-area" name="functional_area" class="form-control">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
    </div>
</div>

<!-- Role -->
<div class="form-group required">
    <label class="col-md-3 control-label">Role <sup>*</sup></label>
    <div class="col-md-8">
        <select id="role" name="role" class="form-control">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
    </div>
</div>

<!-- Desired Job Type -->
<div class="form-group required">
    <label class="col-md-3 control-label">Desired Job Type <sup>*</sup></label>
    <div class="col-md-8">
        <input id="desired-job-type-1" name="desired_job_type" type="radio" value="permanant">Permanant
        <input id="desired-job-type-2" name="desired_job_type" type="radio" value="temporary">Temporary/Contract
        <input id="desired-job-type-3" name="desired_job_type" type="radio" value="both">Both 
    </div>
</div>

<!-- Desired Employment Type -->
<div class="form-group required">
    <label class="col-md-3 control-label">Desired Employment Type <sup>*</sup></label>
    <div class="col-md-8">
        <input id="desired-employment-type-1" name="desired_employment_type" type="radio" value="full">Full Time
        <input id="desired-employment-type-2" name="desired_employment_type" type="radio" value="part">Part Time
        <input id="desired-employment-type-3" name="desired_employment_type" type="radio" value="correspond">Both 
    </div>
</div>

<!-- DoB -->
<div class="form-group required">
    <label class="col-md-3 control-label">Date of Birth <sup>*</sup></label>
    <div class="col-md-8">
        Year:
        <select id="year" name="dob_year" >
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
        Month:
        <select id="month" name="dob_month">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
        Day:
        <select id="day" name="dob_day">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
    </div>
</div>

<!-- Gender -->
<div class="form-group required">
    <label class="col-md-3 control-label">Gender <sup>*</sup></label>
    <div class="col-md-8">
        <input id="gender" name="gender" type="radio" value="male">Male
        <input id="gender" name="gender" type="radio" value="female">Female 
    </div>
</div>

<!-- Marital Status-->
<div class="form-group required">
    <label class="col-md-3 control-label">Marital Status <sup>*</sup></label>
    <div class="col-md-8">
        <select id="marital-status" name="marital_status">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
    </div>
</div>

<!--Mailing Address -->
<div class="form-group required">
    <label class="col-md-3 control-label" for="address">Mailing Address <sup>*</sup></label>
    <div class="col-md-8">
           <textarea class="form-control" id="address" name="address" rows="4" cols="82"> </textarea>
           <br>
           City:<input type="text" name="address_city"> <input type="text" name="address_pin" placeholder="PIN">
    </div>
</div>

<!-- Countries where you are authorise to work-->
<div class="form-group required">
    <label class="col-md-3 control-label">Countries where you are authorise to work <sup>*</sup></label>
    <div class="col-md-8">
        <select id="work-permit-countries" name="work_permit_countries">
            <option value="1">USA</option>
            <option value="2">Canada</option>
            <option value="3">Australia</option>
        </select>
    </div>
</div> 

<!-- Differently Abled -->
<div class="form-group required">
    <label class="col-md-3 control-label">Differently Abled <sup>*</sup></label>
    <div class="col-md-8">
        <input id="disability" name="disability" type="radio" value="male">Yes
        <input id="disability" name="disability" type="radio" value="female">No 
    </div>
</div>

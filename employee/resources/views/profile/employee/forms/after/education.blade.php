<!-- Highest Qualification -->
<div class="form-group required">
    <label class="col-md-3 control-label">Highest Qualification <sup>*</sup></label>
    <div class="col-md-8">
        <input id="highest-qualification" name="highest_qualification" placeholder="Highest Qualification" class="form-control input-md"
                    type="text" value="">
    </div>
</div>

<!-- Course -->
<div class="form-group required">
    <label class="col-md-3 control-label">Course <sup>*</sup></label>
    <div class="col-md-8">
        <input id="course" name="course" placeholder="Course" class="form-control input-md"
                    type="text" value="">
    </div>
</div>

<!-- Specialization -->
<div class="form-group required">
    <label class="col-md-3 control-label">Specialization <sup>*</sup></label>
    <div class="col-md-8">
        <input id="specialization" name="specialization" placeholder="Specialization" class="form-control input-md"
                    type="text" value="">
    </div>
</div>

<!-- University/College Name -->
<div class="form-group required">
    <label class="col-md-3 control-label">University/College Name <sup>*</sup></label>
    <div class="col-md-8">
        <input id="institute" name="institute" placeholder="Institute Name" class="form-control input-md"
                    type="text" value="">
    </div>
</div>

<!-- Course Type -->
<div class="form-group required">
    <label class="col-md-3 control-label">Course Type <sup>*</sup></label>
    <div class="col-md-8">
        <input id="course-type-1" name="course_type" type="radio" value="full">Full Time
        <input id="course-type-2" name="course_type" type="radio" value="part">Part Time
        <input id="course-type-3" name="course_type" type="radio" value="correspond">Correspondence 
    </div>
</div>

<!-- Passing Year -->
<div class="form-group required">
    <label class="col-md-3 control-label">Passing Year <sup>*</sup></label>
    <div class="col-md-8">
        <select name="passing_year" class="form-control selecter"> 
            <option>Year</option>
            <option value="2018">2018</option>
            <option value="2017">2017</option>
            <option value="2016">2016</option>

        </select>
    </div>
</div>


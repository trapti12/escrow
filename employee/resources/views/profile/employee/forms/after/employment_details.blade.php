
<!-- Current Designation -->
<div class="form-group required">
    <label class="col-md-3 control-label">Current Designation <sup>*</sup></label>
    <div class="col-md-8">
        <input id="current-designation" name="current_designation" placeholder="Current Designation" class="form-control input-md"
                    type="text" value="">
    </div>
</div>

<!-- Current Company -->
<div class="form-group required">
    <label class="col-md-3 control-label">Current Company Name <sup>*</sup></label>
    <div class="col-md-8">
        <input id="company-name" name="company" placeholder="Company Name" class="form-control input-md"
                    type="text" value="">
    </div>
</div>

<!-- Working Status -->
<input name="working_status"  type="hidden" value="1">

<!-- Annual Salary -->
<div class="form-group required">
    <label class="col-md-3 control-label">Annual Salary <sup>*</sup></label>
    <div class="col-md-8">
        <input id="current-salary" name="current_salary" placeholder="Annual Salary" class="form-control input-md"
                    type="text" value="">
    </div>
</div>

<!-- Working Since -->
<div class="form-group required">
    <label class="col-md-3 control-label">Working Since <sup>*</sup></label>
    <div class="col-md-8">
        <select id="from-month" name="from_month">
            <option value="Jan">Jan</option>
            <option value="Feb">Feb</option>
            <option value="Mar">Mar</option>
        </select> 
        <select id="from-year" name="from_year">
            <option value="2016">2016</option>
            <option value="2017">2017</option>
            <option value="2018">2018</option>
        </select>
        To
        <select id="to-month" name="to_month">
            <option value="Jan">Jan</option>
            <option value="Feb">Feb</option>
            <option value="Mar">Mar</option>
        </select> 
        <select id="to-year" name="to_year">
            <option value="2016">2016</option>
            <option value="2017">2017</option>
            <option value="2018">2018</option>
        </select>
    </div>
</div>

<!-- Total Experience -->
<div class="form-group required">
    <label class="col-md-3 control-label">Total Experience <sup>*</sup></label>
    <div class="col-md-8">
        <select id="exp-year" name="year_experience">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select> 
        <select id="exp-month" name="month_experience">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
    </div>
</div>

<!--Current Location -->
<div id="cityBox" class="form-group required">
    <label class="col-md-3 control-label" for="current_location">Current Location <sup>*</sup></label>
    <div class="col-md-8">
            <input id="current-location" name="current_location" placeholder="Present Location" class="form-control input-md"
                    type="text" value="">
    </div>
</div>

<!--Description -->
<div class="form-group required">
    <label class="col-md-3 control-label" for="job_description">Description <sup>*</sup></label>
    <div class="col-md-8">
           <textarea class="formcontrol" id="job-description" name="job_description" rows="7" cols="82"> </textarea> 
    </div>
</div>

<!-- tags -->
<div class="form-group">
    <label class="col-md-3 control-label" for="skills">Key Skills</label>
    <div class="col-md-8">
        <input id="skills" name="skills" placeholder="" class="form-control input-md" type="text" value="" multiple>
        <span class="help-block">Enter the skils seperated by commas.</span>
    </div>
</div>

<aside>
	<div class="inner-box">
		<div class="user-panel-sidebar">
            
            @if (isset($user))
                <div class="collapse-box">
                    <h5 class="collapse-title no-border">
                        {{ t('My Account') }}&nbsp;
                        <a href="#MyClassified" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a>
                    </h5>
                    <div class="panel-collapse collapse in" id="MyClassified">
                        <ul class="acc-list">
                            <li>
                                <a {!! ($pagePath=='') ? 'class="active"' : '' !!} href="{{ lurl('account') }}" >
                                    <i class="icon-home"></i> Dashboard
                                </a>
                            </li>
                            @if ($user->user_type_id == 2)
                            <li>
                                <a{!! ($pagePath=='companies') ? ' class="active"' : '' !!} href="{{ lurl('account/companies') }}">
                                <i class="icon-town-hall"></i> {{ t('My companies') }}&nbsp;
                                </a>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
                <!-- /.collapse-box  -->
                @if (!empty($user->user_type_id) and $user->user_type_id != 0 )
                    <div class="collapse-box">
                        <h5 class="collapse-title">
                            {!! ($user->user_type_id == 2) ? ' My Jobs' : 'My Profiles' !!}&nbsp;
                            <a href="#MyAds" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a>
                        </h5>
                        <div class="panel-collapse collapse in" id="MyAds">
                            <ul class="acc-list">
                                @if ($user->user_type_id == 2)
                                <li>
                                    <a{!! ($pagePath=='my-posts') ? ' class="active"' : '' !!} href="{{ lurl('account/my-posts') }}">
                                    <i class="icon-town-hall"></i> Jobs &nbsp;
                                    </a>
                                </li>                               
                                <li>
                                    <a{!! ($pagePath=='pending-approval') ? ' class="active"' : '' !!} href="{{ lurl('account/pending-approval') }}">
                                    <i class="icon-hourglass"></i> {{ t('Pending approval') }}&nbsp;
                                    </a>
                                </li>
                                <li>
                                    <a{!! ($pagePath=='archived') ? ' class="active"' : '' !!} href="{{ lurl('account/archived') }}">
                                    <i class="icon-folder-close"></i> {{ t('Archived ads') }}&nbsp;
                                    </a>
                                </li>
                                 
                                @endif
                                @if ($user->user_type_id == 3)
                                <li>
                                    <a{!! ($pagePath=='employee-profile-list') ? ' class="active"' : '' !!} href="{{ lurl('account/employee-profile-list') }}">
                                    <i class="icon-town-hall"></i> Employee Profile &nbsp;                                    
                                    </a>
                                </li>
                                @endif     
                            </ul>
                        </div>
                    </div>
                    <!-- /.collapse-box  -->
                    @if ($user->user_type_id == 3)
                    <div class="collapse-box">
                        <h5 class="collapse-title">
                            Saved Jobs/History&nbsp;
                            <a href="#MyHistory" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a>
                        </h5>
                        <div class="panel-collapse collapse in" id="MyHistory">
                            <ul class="acc-list">
                                
                                <li>
                                    <a{!! ($pagePath=='favourite') ? ' class="active"' : '' !!} href="{{ lurl('account/favourite') }}">
                                    <i class="icon-docs"></i>  Favourite jobs &nbsp;
                                    </a>
                                </li>

                                <li>
                                    <a{!! ($pagePath=='pending-approval') ? ' class="active"' : '' !!} href="#">
                                    <i class="icon-hourglass"></i> Suggestioins&nbsp;
                                    </a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                    @endif
                    <!-- /.collapse-box  -->


                    <div class="collapse-box">
                        <h5 class="collapse-title">
                            Messages &nbsp;
                            <a href="#MyMessages" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a>
                        </h5>
                        <div class="panel-collapse collapse in" id="MyMessages">
                            <ul class="acc-list">                                
                                <li>
                                    <a{!! ($pagePath=='conversations') ? ' class="active"' : '' !!} href="{{ lurl('account/conversations') }}">
                                    <i class="icon-mail-1"></i> {{ t('Conversations') }}&nbsp;
                                    </a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>

                    <div class="collapse-box">
                        <h5 class="collapse-title">
                            My Subscriptions&nbsp;
                            <a href="#MySubs" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a>
                        </h5>
                        <div class="panel-collapse collapse in" id="MySubs">
                            <ul class="acc-list">                                
                                <li>
                                    <a{!! ($pagePath=='subscriptions') ? ' class="active"' : '' !!} href="#">
                                    <i class="icon-money"></i> Current Subscriptions&nbsp;                                   
                                    </a>
                                </li>
                                <li>
                                    <a{!! ($pagePath=='transactions') ? ' class="active"' : '' !!} href="{{ lurl('account/transactions') }}">
                                    <i class="icon-money"></i> {{ t('Transactions') }}&nbsp;
                                    </a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                                       
                    <!-- /.collapse-box  -->
                
                    <div class="collapse-box">
                        <h5 class="collapse-title">
                            Account Settings&nbsp;
                            <a href="#TerminateAccount" data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a>
                        </h5>
                        <div class="panel-collapse collapse in" id="TerminateAccount">
                            <ul class="acc-list">
                                <li>
                                    <a {!! ($pagePath=='edit') ? 'class="active"' : '' !!} href="{{ lurl('account/edit') }}">
                                        <i class="icon-home"></i> Update Account
                                    </a>
                                </li>
                                @if ($user->user_type_id == 3)
                                 <li>
                                    <a {!! ($pagePath=='blacklist') ? 'class="active"' : '' !!} href="#">
                                        <i class="icon-home"></i> Blacklist Company
                                    </a>
                                </li> 
                                @endif                           
                                <li>
                                    <a {!! ($pagePath=='close') ? 'class="active"' : '' !!} href="#">
                                        <i class="icon-cancel-circled "></i> {{ t('Close account') }}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.collapse-box  -->
                @endif
            @endif

		</div>
	</div>
	<!-- /.inner-box  -->
</aside>
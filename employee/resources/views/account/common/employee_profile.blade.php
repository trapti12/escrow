  
@extends('layouts.master')

@section('content')
	@include('common.spacer')
	<div class="main-container">
		<div class="container">
			<div class="row">

				@if (Session::has('flash_notification'))
					<div class="container" style="margin-bottom: -10px; margin-top: -10px;">
						<div class="row">
							<div class="col-lg-12">
								@include('flash::message')
							</div>
						</div>
					</div>
				@endif

				<div class="col-sm-3 page-sidebar">
					@include('account.common.inc.sidebar')
				</div>
				<!--/.page-sidebar-->

				<div class="col-sm-9 page-content">
					<div class="inner-box">
						@if ($pagePath=='my-posts')
							<h2 class="title-2"><i class="icon-docs"></i> {{ t('My Ads') }} </h2>
						@elseif ($pagePath=='archived')
							<h2 class="title-2"><i class="icon-folder-close"></i> {{ t('Archived ads') }} </h2>
						@elseif ($pagePath=='favourite')
							<h2 class="title-2"><i class="icon-heart-1"></i> {{ t('Favourite jobs') }} </h2>
						@elseif ($pagePath=='pending-approval')
							<h2 class="title-2"><i class="icon-hourglass"></i> {{ t('Pending approval') }} </h2>
						@else
							<h2 class="title-2"><i class="icon-docs"></i> Employee Profiles </h2>
						@endif

						<div class="table-responsive">
							<form name="listForm" method="POST" action="{{ lurl('account/'.$pagePath.'/delete') }}">
								{!! csrf_field() !!}
								<div class="table-action">
									<label for="checkAll">
										<input type="checkbox" id="checkAll">
										{{ t('Select') }}: {{ t('All') }} |
										<button type="submit" class="btn btn-sm btn-default delete-action">
											<i class="fa fa-trash"></i> {{ t('Delete') }}
                                        </button>
									</label>
									<div class="table-search pull-right col-xs-7">
										<div class="form-group">
											<label class="col-xs-5 control-label text-right">{{ t('Search') }} <br>
												<a title="clear filter" class="clear-filter" href="#clear">[{{ t('clear') }}]</a> </label>
											<div class="col-xs-7 searchpan">
												<input type="text" class="form-control" id="filter">
											</div>
										</div>
									</div>
								</div>
								<table id="addManageTable" class="table table-striped table-bordered add-manage-table table demo"
									   data-filter="#filter" data-filter-text-only="true">
									<thead>
									<tr>
										<th data-type="numeric" data-sort-initial="true"></th>
										<th> {{ t('Photo') }}</th>
										<th data-sort-ignore="true"> Profile Details </th>
										<!-- <th data-type="numeric"> --</th> -->
										<th> {{ t('Option') }}</th>
									</tr>
									</thead>
									<tbody>

									<?php 
                                    if (isset($profiles) && $profiles->count() > 0):
									foreach($profiles as $key => $profile):
										// Fixed 1
										if ($pagePath == 'favourite') {
											if (isset($profile->post)) {
												if (!empty($profile->post)) {
													$profile = $profile->post;
												} else {
													continue;
												}
											} else {
												continue;
											}
										}

										$profileUrl = "";

										// Get Post's City
										if ($profile->current_location) {
											$city = $profile->current_location;
										} else {
											$city = '-';
										}
                                        
										// Get country flag
										$iconPath = 'images/flags/16/' . strtolower($profile->country_code) . '.png';
									?>
									<tr>
										<td style="width:2%" class="add-img-selector">
											<div class="checkbox">
												<label><input type="checkbox" name="entries[]" value="{{ $profile->id }}"></label>
											</div>
										</td>
										<td style="width:14%" class="add-img-td">
											<a href="{{ $profileUrl }}">
												<img class="thumbnail img-responsive" src="{{ resize($profile->photo, 'medium') }}" alt="img">
											</a>
										</td>
										<td style="width:58%" class="ads-details-td">
											<div>
												
												<p>
                                                    <strong>
                                                        <a href="{{ $profileUrl }}" title="{{ $profile->profile_name }}">{{ str_limit($profile->profile_name, 40) }}</a>
                                                    </strong>                                                    
                                                </p>
												<p>
													<strong><i class="icon-clock" title="{{ t('Posted On') }}"></i></strong>&nbsp;
													{{ $profile->created_at->formatLocalized(config('settings.app.default_datetime_format')) }}
												</p>
												<p>
													<strong><i class="icon-eye" title="{{ t('Visitors') }}"></i></strong> {{ $profile->visits or 0 }}
													<strong><i class="fa fa-map-marker" title="{{ t('Located In') }}"></i></strong> {{ $city }}
													@if (file_exists(public_path($iconPath)))
														<img src="{{ url($iconPath) }}" data-toggle="tooltip" title="{{ $profile->country_code }}">
													@endif
												</p>
											</div>
										</td>
										
										<td style="width:10%" class="action-td">
											<div>
												@if ($profile->user_id==$user->id and $profile->archived==0) 
													<p>
                                                        <a class="btn btn-primary btn-sm" href="{{ lurl('profile/' . $profile->id . '/edit-basic') }}">
                                                            <i class="fa fa-edit"></i> {{ t('Edit') }}
                                                        </a>
                                                    </p>
												@endif
												@if (isVerifiedPost($profile) and $profile->archived==0)
													<!--<p>
														<a class="btn btn-info btn-sm"> <i class="fa fa-mail-forward"></i> {{ t('Share') }} </a>
													</p>-->
												@endif
												@if ($profile->user_id==$user->id and $profile->archived==1)
													<p>
                                                        <a class="btn btn-info btn-sm" href="{{ lurl('account/'.$pagePath.'/'.$profile->id.'/repost') }}">
                                                            <i class="fa fa-recycle"></i> {{ t('Repost') }}
                                                        </a>
                                                    </p>
												@endif
												<p>
                                                    <a class="btn btn-danger btn-sm delete-action" href="{{ lurl('profile/'.$profile->id.'/delete') }}">
                                                        <i class="fa fa-trash"></i> {{ t('Delete') }}
                                                    </a>
                                                </p>
											</div>
										</td>
									</tr>
									<?php endforeach; ?>
                                    <?php endif; ?>
									</tbody>
								</table>
							</form>
						</div>
							
                        <div class="pagination-bar text-center">
                            {{ (isset($profiles)) ? $profiles->links() : '' }}
                        </div>

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('after_scripts')
	<script src="{{ url('assets/js/footable.js?v=2-0-1') }}" type="text/javascript"></script>
	<script src="{{ url('assets/js/footable.filter.js?v=2-0-1') }}" type="text/javascript"></script>
	<script type="text/javascript">
		$(function () {
			$('#addManageTable').footable().bind('footable_filtering', function (e) {
				var selected = $('.filter-status').find(':selected').text();
				if (selected && selected.length > 0) {
					e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
					e.clear = !e.filter;
				}
			});

			$('.clear-filter').click(function (e) {
				e.preventDefault();
				$('.filter-status').val('');
				$('table.demo').trigger('footable_clear_filter');
			});

			$('#checkAll').click(function () {
				checkAll(this);
			});
			
			$('a.delete-action, button.delete-action').click(function(e)
			{
				e.preventDefault(); /* prevents the submit or reload */
				var confirmation = confirm("{{ t('Are you sure you want to perform this action?') }}");
				
				if (confirmation) {
					if( $(this).is('a') ){
						var url = $(this).attr('href');
						if (url !== 'undefined') {
							redirect(url);
						}
					} else {
						$('form[name=listForm]').submit();
					}
					
				}
				
				return false;
			});
		});
	</script>
	<!-- include custom script for ads table [select all checkbox]  -->
	<script>
		function checkAll(bx) {
			var chkinput = document.getElementsByTagName('input');
			for (var i = 0; i < chkinput.length; i++) {
				if (chkinput[i].type == 'checkbox') {
					chkinput[i].checked = bx.checked;
				}
			}
		}
	</script>
@endsection

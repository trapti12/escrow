<div class="row"> 
@foreach($data as $key => $row)
<div style="overflow: hidden; padding: 25px;" class="col-sm-6">
			<div class="col-sm-10 col-xs-10 add-desc-box">
				<div class="add-details jobs-item">
					<h4 class="job-title">
						<a href="{{ lurl('employee-details/'.$row->id.'/'.$row->resume_heading) }}"> {!! $row->resume_heading !!} </a>
					</h4>
					<span class="info-row">
						<span class="date">
							<i class="icon-clock"> </i>
							{{ $row->created_at->diffForHumans() }}
						</span>
						<span class="item-location">
							<i class="fa fa-map-marker"></i>
							<a href="">
								{{ $row->city->name }}
							</a>
						</span>
						<span class="item-location">
							<i class="fa icon-tag"></i>
							<a href="">
								{{ $row->jobtype->name }}
							</a>
						</span>
						<span class="salary">
							<i class="icon-money"> </i>
							CTC : {{ $salary_type[$row->annual_salary] }} , 
							ECTC : {{ $salary_type[$row->expected_annual_salary] }}
							
						</span>
					</span>
					<div class="jobs-desc">
						{!! $row->employment_details !!}
					</div>

				</div>
			</div>
		</div>
@endforeach
</div>
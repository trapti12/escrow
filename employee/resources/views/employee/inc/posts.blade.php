<?php
if (!isset($cacheExpiration)) {
    $cacheExpiration = (int)config('settings.other.cache_expiration');
}
?>
@if (isset($paginator) and $paginator->getCollection()->count() > 0)
	<?php
    if (!isset($cats)) {
        $cats = collect([]);
    }
    
	foreach($paginator->getCollection() as $key => $profile):
		if (!$countries->has($profile->country_code)) continue;
	
		// Convert the created_at date to Carbon object
		$profile->created_at = \Date::parse($profile->created_at)->timezone(config('timezone.id'));
		$profile->created_at = $profile->created_at->ago();
		
		// Category
		$cacheId = 'category.' . $profile->industry_id . '.' . config('app.locale');
		$liveCat = \Illuminate\Support\Facades\Cache::remember($cacheId, $cacheExpiration, function () use ($profile) {
			$liveCat = \App\Models\Category::find($profile->industry_id);
			return $liveCat;
		});
		
		// Get the profile's City
		$cacheId = config('country.code') . '.city.' . $profile->current_location;
		$city = \Illuminate\Support\Facades\Cache::remember($cacheId, $cacheExpiration, function () use ($profile) {
			$city = \App\Models\City::find($profile->current_location);
			return $city;
		});
		//if (empty($city)) continue;
		/* if ($counter % 2 != 0) {
            echo '<div class="row">'; style="overflow: hidden; padding: 15px;" class="col-sm-6" 
        } */
		?>
		<div class="item-list job-item">
			<div class="col-sm-1 col-xs-2 no-padding photobox">
				<div class="add-image">
					<a href="{{ lurl($profile->uri) }}">
						<img class="thumbnail no-margin" src="{{ resize($profile->photo, 'medium') }}" alt="{{ mb_ucfirst($profile->name) }}">
					</a>
				</div>
			</div>
			<!--/.photobox-->
			<div class="col-sm-10 col-xs-10 add-desc-box">
				<div class="add-details jobs-item">
					
					<h5 class="company-title">
						<a href="{{ lurl($profile->uri) }}"> {{ mb_ucfirst(str_limit($profile->name, 70)) }} </a>
					</h5>

					<h4 class="job-title">
						<a href="{{ lurl($profile->uri) }}"> {{ mb_ucfirst(str_limit($profile->profile_name, 70)) }} </a>
					</h4>

					<span class="info-row">
						<span class="date">
							<i class="icon-clock"> </i>
							{{ $profile->created_at }}
						</span>
											
						<span class="salary">
							<i class="icon-money"> </i>
							
							{{!!$profile->current_salary!!}}
						</span>
					</span>

					<div class="jobs-desc">
						{!! str_limit(strCleaner($profile->profile_summary), 180) !!}
					</div>

					<div class="job-actions">
						<ul class="list-unstyled list-inline">
							@if (auth()->check())
								@if (1)
								<li id="{{ $profile->id }}">
									<a class="save-job" id="save-{{ $profile->id }}">
										<span class="fa fa-heart-o"></span>
										{{ t('Save Job') }}
									</a>
								</li>
								@else
								<li class="saved-job" id="{{ $profile->id }}">
									<a class="saved-job" id="saved-{{ $profile->id }}">
										<span class="fa fa-heart"></span>
										{{ t('Saved Job') }}
									</a>
								</li>
								@endif
							@else
								<li id="{{ $profile->id }}">
									<a class="save-job" id="save-{{ $profile->id }}">
										<span class="fa fa-heart-o"></span>
										{{ t('Save Job') }}
									</a>
								</li>
							@endif
							<li>
								<a class="email-job" data-toggle="modal" data-id="{{ $profile->id }}" href="#sendByEmail" id="email-{{ $profile->id }}">
									<i class="fa fa-envelope"></i>
									{{ t('Email Job') }}
								</a>
							</li>
						</ul>
					</div>

				</div>
			</div>
			<!--/.add-desc-box-->

			<!--/.add-desc-box-->
		</div>
		<!--/.job-item-->
	<?php 
		/* if ($counter % 2 == 0 || $length == $counter) {
          	echo '</div>';
        }

        $counter++; */

	endforeach; ?>

@else
	<div class="item-list">
		@if (str_contains(\Route::currentRouteAction(), 'Search\CompanyController'))
			{{ t('No jobs were found for this company.') }}
		@else
			{{ t('No result. Refine your search using other criteria.') }}
		@endif
	</div>
@endif

@section('modal_location')
	@parent
	@include('layouts.inc.modal.send-by-email')
@endsection

@section('after_scripts')
	@parent
	<script>
		/* Favorites Translation */
		var lang = {
			labelSaveprofileSave: "{!! t('Save Job') !!}",
			labelSaveprofileRemove: "{{ t('Saved Job') }}",
			loginToSaveprofile: "{!! t('Please log in to save the Ads.') !!}",
			loginToSaveSearch: "{!! t('Please log in to save your search.') !!}",
			confirmationSaveprofile: "{!! t('profile saved in favorites successfully !') !!}",
			confirmationRemoveSaveprofile: "{!! t('profile deleted from favorites successfully !') !!}",
			confirmationSaveSearch: "{!! t('Search saved successfully !') !!}",
			confirmationRemoveSaveSearch: "{!! t('Search deleted successfully !') !!}"
		};
		
		$(document).ready(function ()
		{
			/* Get profile ID */
			$('.email-job').click(function(){
				var profileId = $(this).attr("data-id");
				$('input[type=hidden][name=profile]').val(profileId);
			});

			@if (isset($errors) and $errors->any())
				@if (old('sendByEmailForm')=='1')
					$('#sendByEmail').modal();
				@endif
			@endif
		})
	</script>
@endsection

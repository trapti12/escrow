  
<?php
	$fullUrl = url(\Illuminate\Support\Facades\Request::getRequestUri());
    $tmpExplode = explode('?', $fullUrl);
    $fullUrlNoParams = current($tmpExplode);
?>
@extends('layouts.master')



@section('content')

<div class="main-container">
	<div class="h-spacer"></div>
		<div class="container">
			<ol class="breadcrumb pull-left">
				<li><a href="http://dev.fairconnects.localhost"><i class="icon-home fa"></i></a></li>
				<!-- <li><a href="http://localhost/new/fairconnects/public">{{ 'india' }}</a></li> -->
				<li>
				<a href="#">
						
					</a>
				</li>
				<li class="active">{{ $profile->profile_name }}</li>
			</ol>
			<div class="pull-right backtolist">
				<a href="{{ url('latest-employees') }}">
					<i class="fa fa-angle-double-left"></i> Back to Results
				</a>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-sm-9 page-content col-thin-right">
					<div class="inner inner-box ads-details-wrapper">
						<h2 class="enable-long-words">
							<strong>
                                <a href="#" title="XxhPOHNd2T">
                                   {!! $profile->name !!}
                                </a>
                            </strong>
							<small class="label label-default adlistingtype">{{ $profile->profile_name }}</small>
                            						</h2>
						<span class="info-row">
							<span class="date"><i class=" icon-clock"> </i> </span> -&nbsp;
							<span class="item-location"><i class="fa fa-map-marker"></i>  </span> -&nbsp;
							<!-- <span class="category"><i class="icon-eye-3"></i> 0 view</span> -->
						</span>

						<div class="ads-details">
							<div class="row" style="padding-bottom: 20px;">
								<div class="ads-details-info jobs-details-info col-md-8 enable-long-words from-wysiwyg">
									<h5 class="list-title"><strong>Profile Summary</strong></h5>
									
									<!-- Description -->
                                    <div>
										<p>{!! $profile->profile_summary !!}</p>
                                    </div>
									
																			<!-- Company Description -->
										<div style="margin-bottom: 50px;"></div>
										<h5 class="list-title"><strong>Employment Details</strong></h5>
                                        <div>
										    {!!  !!}
                                        </div>

										
                                        <!-- Company Description -->
										<div style="margin-bottom: 50px;"></div>
										<h5 class="list-title"><strong>Resume</strong></h5>
                                        <div>
                                        	<iframe height="300px;" width="100%;" name="myiframe" id="myiframe" src="{{  $data->getResume() }}"></iframe>
										  
                                        </div>

                                        <a href="{{  $data->getResume() }}" target="_blank"> Download </a>
										@endif	
									<!-- Tags -->
									<!-- <div style="clear: both;"></div>
											<div class="tags">
												<h5 class="list-title"><strong>Tags</strong></h5>						<a href="http://localhost/new/fairconnects/public/tag/lb4jlgfzmb">
														lb4jlgfzmb
													</a>
												</div>	 -->											
											</div>
								<div class="col-md-4">
									<aside class="panel panel-body panel-details job-summery">
										<ul>
											<li>
												<p class="no-margin">
												<strong>Heading:</strong>&nbsp;	
												<a href="#">{!! $data->resume_heading !!}</a>
												</p>
											</li>
											<li>
												<p class="no-margin">
												<strong>Current Designation:</strong>&nbsp;	
												{!! $data->current_designation !!}
												</p>
											</li>
											<li>
												<p class="no-margin">
												<strong>Total Experience:</strong>&nbsp;	
												{!! $total_exp[$data->total_experience] !!}
												</p>
											</li>
											<li>
												<p class="no-margin">
							<strong> CTC : </strong>
							{!!  !!}
							  </p>
							</li>
							<li>
							 <p class="no-margin">
							  <strong> ECTC : </strong>{!!  !!}	</p>	
							</li>
											<li>																																			<p class="no-margin">
													<strong>Job Type:</strong>&nbsp;																										<a href="{{ url('employee-details/'.$data->id.'/'.$data->resume_heading) }}">
														{{ $profile->profile_name }}
													</a>												</p>																							</li>											<li>
												<p class="no-margin">													<strong>Location:</strong>&nbsp;																										<a href="#">
														{{ $profile->current_location }}
													</a>
												</p>
											</li>
										</ul>
									</aside>
									<!-- <div class="ads-action">
										<ul class="list-border">																						<li>																										<a href="http://localhost/new/fairconnects/public/companies/1/jobs">
														<i class="fa icon-town-hall"></i> More jobs by Comapny
													</a>
												</li>																																	<li>																										<a href="http://localhost/new/fairconnects/public/users/1/jobs">
														<i class="fa fa-user"></i> More jobs by sanat
													</a>
												</li>
																						<li id="42">
												<a class="make-favorite">																																							<i class="fa fa-heart-o"></i> Save Job																									                                                </a>
											</li>
											<li>
												<a href="http://localhost/new/fairconnects/public/posts/42/report">
													<i class="fa icon-info-circled-alt"></i> Report abuse
												</a>
											</li>
										</ul>
									</div> -->
								</div>

								<br>&nbsp;<br>
							</div>
							@if(Auth::check() && Auth::user()->user_id == $data->user_id)
							<div class="content-footer text-left">																											<a class="btn btn-default" href="#">
											<i class="fa fa-pencil-square-o"></i> Edit
										</a>																																	&nbsp;
							</div>
							@endif
						</div>
					</div>
					<!--/.ads-details-wrapper-->
				</div>
				<!--/.page-content-->

				<!-- <div class="col-sm-3 page-sidebar-right">
					<aside>
						<div class="panel sidebar-panel panel-contact-seller">
							<div class="panel-heading">Company Information</div>
							<div class="panel-content user-info">
								<div class="panel-body text-center">
									<div class="seller-info">
										<div class="company-logo-thumb mb20">																																			<a href="#">
													<img alt="Logo Ud1Eg7dEjg" class="img-responsive" src="#">
												</a>
																					</div>
																					<h3 class="no-margin">
																						<a href="#">
													{{ $data->name }}
												</a>
											</h3>													<p>
											Location:&nbsp;											<strong>																								<a href="#">
													{{ $data->city->name }}
												</a>
											</strong>
										</p>																																					
										@if(Auth::check() && Auth::user()->user_id == $data->user_id)													</div>
									<div class="user-ads-action">																																	<a href="" data-toggle="modal" class="btn btn-default btn-block">
													<i class="fa fa-pencil-square-o"></i> Edit
												</a>																																								</div>
												@endif
								</div>
							</div>
						</div>	 -->					
												
													<!-- <div style="margin: 25px 0; text-align: center;">
	<button class="btn btn-fb share s_facebook" href="https://www.facebook.com/sharer.php?s=100&amp;p[title]=XxhPOHNd2T%2C%20Indore&amp;u=http%3A%2F%2Flocalhost%2Fnew%2Ffairconnects%2Fpublic%2Fnew%2Ffairconnects%2Fpublic%2Fxxhpohnd2t%2F42.html&amp;t=XxhPOHNd2T%2C%20Indore&amp;p[summary]=XxhPOHNd2T%2C%20Indore&amp;p[url]=http%3A%2F%2Flocalhost%2Fnew%2Ffairconnects%2Fpublic%2Fnew%2Ffairconnects%2Fpublic%2Fxxhpohnd2t%2F42.html"><i class="icon-facebook"></i> </button>&nbsp;
	<button class="btn btn-tw share s_twitter" href="https://twitter.com/intent/tweet?url=http%3A%2F%2Flocalhost%2Fnew%2Ffairconnects%2Fpublic%2Fnew%2Ffairconnects%2Fpublic%2Fxxhpohnd2t%2F42.html&amp;text=XxhPOHNd2T%2C%20Indore"><i class="icon-twitter"></i> </button>&nbsp;
	<button class="btn btn-danger share s_plus" href="https://plus.google.com/share?url=http%3A%2F%2Flocalhost%2Fnew%2Ffairconnects%2Fpublic%2Fnew%2Ffairconnects%2Fpublic%2Fxxhpohnd2t%2F42.html"><i class="icon-googleplus-rect"></i> </button>&nbsp;
	<button class="btn btn-lin share s_linkedin" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=http%3A%2F%2Flocalhost%2Fnew%2Ffairconnects%2Fpublic%2Fnew%2Ffairconnects%2Fpublic%2Fxxhpohnd2t%2F42.html&amp;title=XxhPOHNd2T%2C%20Indore&amp;summary=XxhPOHNd2T%2C%20Indore&amp;source=http%3A%2F%2Flocalhost%2Fnew%2Ffairconnects%2Fpublic%2Fnew%2Ffairconnects%2Fpublic%2Fxxhpohnd2t%2F42.html"><i class="icon-linkedin"></i> </button>
</div>		 -->				
						<!-- <div class="panel sidebar-panel">
							<div class="panel-heading">Tips for candidates</div>
							<div class="panel-content">
								<div class="panel-body text-left">
									<ul class="list-check">
										<li> Check if the offer matches your profile </li>
                                        <li> Check the start date </li>
										<li> Meet the employer in a professional location </li>
									</ul>
                                    																	</div>
							</div>
						</div> -->
					</aside>
				</div>
			</div>

		</div>
		
		<div class="h-spacer"></div>
		<div class="container">
		
	</div>								
	</div>
	
@endsection
@section('after_scripts')

@endsection

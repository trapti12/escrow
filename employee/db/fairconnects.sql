-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2018 at 12:28 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fairconnects-refine`
--

-- --------------------------------------------------------

--
-- Table structure for table `ee_profile`
--

CREATE TABLE `ee_profile` (
  `id` int(10) NOT NULL,
  `country_code` varchar(2) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `profile_name` varchar(255) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `verified_email` tinyint(1) DEFAULT NULL,
  `verified_phone` tinyint(1) DEFAULT NULL,
  `email_token` varchar(32) DEFAULT NULL,
  `phone_token` varchar(32) DEFAULT NULL,
  `resume_headline` varchar(255) DEFAULT NULL,
  `highest_qualification` varchar(50) DEFAULT NULL,
  `total_experience` varchar(10) DEFAULT NULL,
  `ee_profile_type` int(2) NOT NULL,
  `industry_id` int(5) DEFAULT NULL,
  `functional_area_id` int(5) DEFAULT NULL,
  `role_id` int(5) DEFAULT NULL,
  `current_location` int(10) DEFAULT NULL,
  `prefered_location` int(10) DEFAULT NULL,
  `lon` float DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `skills` varchar(255) DEFAULT NULL,
  `current_salary` float DEFAULT NULL,
  `notice_period` int(3) DEFAULT NULL,
  `notice_period_status` tinyint(1) DEFAULT NULL,
  `expected_salary` float DEFAULT NULL,
  `it_skills` text,
  `profile_summary` text,
  `photo` varchar(255) DEFAULT NULL,
  `resume` varchar(255) DEFAULT NULL,
  `downloads` int(10) DEFAULT NULL,
  `visits` int(10) DEFAULT NULL,
  `archived` tinyint(1) DEFAULT '0',
  `reviewed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ee_profile`
--

INSERT INTO `ee_profile` (`id`, `country_code`, `user_id`, `profile_name`, `name`, `email`, `phone`, `verified_email`, `verified_phone`, `email_token`, `phone_token`, `resume_headline`, `highest_qualification`, `total_experience`, `ee_profile_type`, `industry_id`, `functional_area_id`, `role_id`, `current_location`, `prefered_location`, `lon`, `lat`, `skills`, `current_salary`, `notice_period`, `notice_period_status`, `expected_salary`, `it_skills`, `profile_summary`, `photo`, `resume`, `downloads`, `visits`, `archived`, `reviewed`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'IN', 3, 'Content Writer', 'Syamal Ch', 'user2@gmail.com', '23432324324', 1, 1, NULL, NULL, NULL, NULL, '1,1', 1, 1, 1, 1, 0, NULL, NULL, NULL, 'php,html,css', 5, NULL, NULL, NULL, NULL, 'asasdsadsad sadas da asd sadasd', 'employee/photos/in/1/8c6c1f3fdb1824d004be0d50db302156.jpg', 'employee/resumes/in/1/c34d8e290033897a8644ec6c87ab7633.jpg', NULL, NULL, 0, 0, '2018-04-09 04:06:54', '2018-04-09 04:07:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ee_profile_education`
--

CREATE TABLE `ee_profile_education` (
  `id` int(10) NOT NULL,
  `ee_profile_id` int(10) NOT NULL,
  `course` varchar(50) NOT NULL,
  `specialization` varchar(50) NOT NULL,
  `institute` varchar(200) NOT NULL,
  `course_type` varchar(10) NOT NULL,
  `passing_year` varchar(5) NOT NULL,
  `grading_system` varchar(10) DEFAULT NULL,
  `marks` int(5) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ee_profile_education`
--

INSERT INTO `ee_profile_education` (`id`, `ee_profile_id`, `course`, `specialization`, `institute`, `course_type`, `passing_year`, `grading_system`, `marks`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'B.tech', 'CSE', 'MVGR', 'full', '2016', NULL, NULL, '2018-04-09 04:06:55', '2018-04-09 04:06:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ee_profile_employment`
--

CREATE TABLE `ee_profile_employment` (
  `id` int(10) NOT NULL,
  `ee_profile_id` int(10) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `company` varchar(70) NOT NULL,
  `from_date` varchar(10) NOT NULL,
  `to_date` varchar(10) NOT NULL,
  `description` text NOT NULL,
  `working_status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ee_profile_employment`
--

INSERT INTO `ee_profile_employment` (`id`, `ee_profile_id`, `designation`, `company`, `from_date`, `to_date`, `description`, `working_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Sr. Developer', 'Fair Connects', 'Jan,2016', 'Jan,2016', 'SDsdsdsdsdSdsdsdsds sds dasd ad sadsa d sad', 1, '2018-04-09 04:06:55', '2018-04-09 04:06:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ee_profile_extra`
--

CREATE TABLE `ee_profile_extra` (
  `id` int(10) NOT NULL,
  `ee_profile_id` int(10) NOT NULL,
  `dob` date DEFAULT NULL,
  `marital_status` tinyint(1) DEFAULT NULL,
  `gender` varchar(5) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `desired_job_type` varchar(10) DEFAULT NULL,
  `desired_employment_type` varchar(10) DEFAULT NULL,
  `work_permit_countries` varchar(100) DEFAULT NULL,
  `disability` varchar(5) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ee_profile_extra`
--

INSERT INTO `ee_profile_extra` (`id`, `ee_profile_id`, `dob`, `marital_status`, `gender`, `address`, `desired_job_type`, `desired_employment_type`, `work_permit_countries`, `disability`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '0001-01-01', NULL, NULL, 'werewrer, sds , fdsf d,sd dsadsad-123455', 'permanant', 'full', '1', 'femal', '2018-04-09 04:07:53', '2018-04-09 04:07:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ee_settings`
--

CREATE TABLE `ee_settings` (
  `user_id` int(11) NOT NULL,
  `default_profile_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ee_settings`
--

INSERT INTO `ee_settings` (`user_id`, `default_profile_id`) VALUES
(3, 1),
(3, 1),
(3, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ee_profile`
--
ALTER TABLE `ee_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ee_profile_education`
--
ALTER TABLE `ee_profile_education`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ee_profile_id` (`ee_profile_id`);

--
-- Indexes for table `ee_profile_employment`
--
ALTER TABLE `ee_profile_employment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ee_profile_id` (`ee_profile_id`);

--
-- Indexes for table `ee_profile_extra`
--
ALTER TABLE `ee_profile_extra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ee_profile_id` (`ee_profile_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ee_profile`
--
ALTER TABLE `ee_profile`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ee_profile_education`
--
ALTER TABLE `ee_profile_education`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ee_profile_employment`
--
ALTER TABLE `ee_profile_employment`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ee_profile_extra`
--
ALTER TABLE `ee_profile_extra`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

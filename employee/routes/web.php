<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Back-end
|--------------------------------------------------------------------------
|
| The admin panel routes
|
*/

Route::group([
    'middleware' => ['admin', 'bannedUser', 'preventBackHistory'],
    'prefix'     => config('larapen.admin.route_prefix', 'admin'),
    'namespace'  => 'App\Http\Controllers\Admin',
], function() {
    // CRUD
    CRUD::resource('advertising', 'AdvertisingController');
    CRUD::resource('blacklist', 'BlacklistController');
    CRUD::resource('category', 'CategoryController');
    CRUD::resource('category/{catId}/sub_category', 'SubCategoryController');
    CRUD::resource('city', 'CityController');
	CRUD::resource('company', 'CompanyController');
    CRUD::resource('country', 'CountryController');
    CRUD::resource('country/{countryCode}/city', 'CityController');
    CRUD::resource('country/{countryCode}/loc_admin1', 'SubAdmin1Controller');
    CRUD::resource('currency', 'CurrencyController');
    CRUD::resource('gender', 'GenderController');
    CRUD::resource('homepage_section', 'HomeSectionController');
    CRUD::resource('loc_admin1/{admin1Code}/city', 'CityController');
    CRUD::resource('loc_admin1/{admin1Code}/loc_admin2', 'SubAdmin2Controller');
    CRUD::resource('loc_admin2/{admin2Code}/city', 'CityController');
    CRUD::resource('meta_tag', 'MetaTagController');
    CRUD::resource('package', 'PackageController');
    CRUD::resource('page', 'PageController');
    CRUD::resource('payment', 'PaymentController');
    CRUD::resource('payment_method', 'PaymentMethodController');
    CRUD::resource('picture', 'PictureController');
    CRUD::resource('post', 'PostController');
    CRUD::resource('p_type', 'PostTypeController');
    CRUD::resource('report_type', 'ReportTypeController');
    CRUD::resource('salary_type', 'SalaryTypeController');
    CRUD::resource('time_zone', 'TimeZoneController');
    CRUD::resource('user', 'UserController');
    CRUD::resource('employers', 'EmployersController');
    CRUD::resource('emplprofile', 'EmplprofileController');
    CRUD::resource('jobseekers', 'JobSeekerController');
    CRUD::resource('transactions', 'TransactionsController');

    //New CRUDs for Packages
    CRUD::resource('/packages/employee', 'EmployeePackagesController');
    CRUD::resource('/packages/employer', 'EmployerPackagesController');

    //New CRUDs for System - Industries
    CRUD::resource('/industry', 'IndustryController');
    CRUD::resource('/functional-area', 'FunctionalAreaController');
    CRUD::resource('/role', 'RoleController');
    CRUD::resource('/course', 'CourseController');
    CRUD::resource('/specialization', 'SpecializationController');

    // Others
    Route::get('account', 'UserController@account');
    Route::post('ajax/{table}/{field}', 'AjaxController@saveAjaxRequest');
    Route::get('clear_cache', 'CacheController@clear');
	Route::get('test_cron', 'TestCronController@run');
	
	// Maintenance Mode
	Route::post('maintenance/down', 'MaintenanceController@down');
	Route::get('maintenance/up', 'MaintenanceController@up');
    
    // Re-send Email or Phone verification message
    Route::get('verify/user/{id}/resend/email', 'UserController@reSendVerificationEmail');
    Route::get('verify/user/{id}/resend/sms', 'UserController@reSendVerificationSms');
    Route::get('verify/post/{id}/resend/email', 'PostController@reSendVerificationEmail');
    Route::get('verify/post/{id}/resend/sms', 'PostController@reSendVerificationSms');

    // Plugins
    Route::get('plugin', 'PluginController@index');
    Route::get('plugin/{plugin}/install', 'PluginController@install');
    Route::get('plugin/{plugin}/uninstall', 'PluginController@uninstall');
    Route::get('plugin/{plugin}/delete', 'PluginController@delete');
});


/*
|--------------------------------------------------------------------------
| Front-end
|--------------------------------------------------------------------------
|
| The not translated front-end routes
|
*/
Route::group([
    'middleware' => ['web'],
    'namespace'  => 'App\Http\Controllers',
], function($router) {
    // AJAX
    Route::group(['prefix' => 'ajax'], function($router) {
        Route::get('countries/{countryCode}/admins/{adminType}', 'Ajax\LocationController@getAdmins');
        Route::get('countries/{countryCode}/admins/{adminType}/{adminCode}/cities', 'Ajax\LocationController@getCities');
        Route::get('countries/{countryCode}/cities/{id}', 'Ajax\LocationController@getSelectedCity');
        Route::post('countries/{countryCode}/cities/autocomplete', 'Ajax\LocationController@searchedCities');
        Route::post('countries/{countryCode}/admin1/cities', 'Ajax\LocationController@getAdmin1WithCities');
        Route::post('category/sub-categories', 'Ajax\CategoryController@getSubCategories');
        Route::post('save/post', 'Ajax\PostController@savePost');
        Route::post('save/search', 'Ajax\PostController@saveSearch');
        Route::post('post/phone', 'Ajax\PostController@getPhone');
    });

    // SEO
    Route::get('sitemaps.xml', 'SitemapsController@index');
	
	// Impersonate (As admin user, login as an another user)
	Route::group(['middleware' => 'auth'], function ($router) {
		Route::impersonate();
	});
});


/*
|--------------------------------------------------------------------------
| Front-end
|--------------------------------------------------------------------------
|
| The translated front-end routes
|
*/
Route::group([
    'prefix'     => LaravelLocalization::setLocale(),
    'middleware' => ['local'],
    'namespace'  => 'App\Http\Controllers',
], function($router) {
    Route::group(['middleware' => 'web'], function($router) {
        // HOMEPAGE
        Route::group(['middleware' => 'httpCache:yes'], function($router) {
            Route::get('test',function(){
                print_r(App\Models\User::find(1)->gender());             });
            Route::get('/', 'HomeController@index');
            Route::get(LaravelLocalization::transRoute('routes.countries'), 'CountriesController@index');
        });


        // AUTH
        Route::group(['middleware' => ['guest', 'preventBackHistory']], function() {
            // Registration Routes...
            Route::get(LaravelLocalization::transRoute('routes.register'), 'Auth\RegisterController@showRegistrationForm');
            Route::post(LaravelLocalization::transRoute('routes.register'), 'Auth\RegisterController@register');
            Route::get('register/finish', 'Auth\RegisterController@finish');

            // Registration Routes for Employer...
            Route::get('register/employer', 'Auth\EmployerRegisterController@showRegistrationForm');
            Route::post('register/employer', 'Auth\EmployerRegisterController@register');
            Route::get('register/employer/finish', 'Auth\EmployerRegisterController@finish');

            // Authentication Routes...
            Route::get(LaravelLocalization::transRoute('routes.login'), 'Auth\LoginController@showLoginForm');
            Route::post(LaravelLocalization::transRoute('routes.login'), 'Auth\LoginController@login');
	
			// Forgot Password Routes...
			Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
			Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
	
			// Reset Password using Token
			Route::get('password/token', 'Auth\ForgotPasswordController@showTokenRequestForm');
			Route::post('password/token', 'Auth\ForgotPasswordController@sendResetToken');
	
			// Reset Password using Link (Core Routes...)
			Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
			Route::post('password/reset', 'Auth\ResetPasswordController@reset');

            // Social Authentication
            Route::get('auth/facebook', 'Auth\SocialController@redirectToProvider');
            Route::get('auth/facebook/callback', 'Auth\SocialController@handleProviderCallback');
            Route::get('auth/google', 'Auth\SocialController@redirectToProvider');
            Route::get('auth/google/callback', 'Auth\SocialController@handleProviderCallback');
            Route::get('auth/twitter', 'Auth\SocialController@redirectToProvider');
            Route::get('auth/twitter/callback', 'Auth\SocialController@handleProviderCallback');
        });

        // Email Address or Phone Number verification
		$router->pattern('field', 'email|phone');
        Route::get('verify/user/{id}/resend/email', 'Auth\RegisterController@reSendVerificationEmail');
		Route::get('verify/user/{id}/resend/sms', 'Auth\RegisterController@reSendVerificationSms');
        Route::get('verify/user/{field}/{token?}', 'Auth\RegisterController@verification');
        Route::post('verify/user/{field}/{token?}', 'Auth\RegisterController@verification');

        // User Logout
        Route::get(LaravelLocalization::transRoute('routes.logout'), 'Auth\LoginController@logout');


        // POSTS
        Route::group(['namespace' => 'Post'], function($router) {
            $router->pattern('id', '[0-9]+');
			// $router->pattern('slug', '.*');
			$router->pattern('slug', '^(?=.*)((?!\/).)*$');
			
            Route::get('posts/create/{tmpToken?}', 'CreateController@getForm');
            Route::post('posts/create', 'CreateController@postForm');
            Route::put('posts/create/{tmpToken}', 'CreateController@postForm');
            Route::get('posts/create/{tmpToken}/packages', 'PackageController@getForm');
            Route::post('posts/create/{tmpToken}/packages', 'PackageController@postForm');
            Route::get('posts/create/{tmpToken}/finish', 'CreateController@finish');
    
            // Payment Gateway Success & Cancel
            Route::get('posts/create/{tmpToken}/payment/success', 'PackageController@paymentConfirmation');
            Route::get('posts/create/{tmpToken}/payment/cancel', 'PackageController@paymentCancel');
    
            // Email Address or Phone Number verification
            $router->pattern('field', 'email|phone');
            Route::get('verify/post/{id}/resend/email', 'CreateController@reSendVerificationEmail');
            Route::get('verify/post/{id}/resend/sms', 'CreateController@reSendVerificationSms');
            Route::get('verify/post/{field}/{token?}', 'CreateController@verification');
            Route::post('verify/post/{field}/{token?}', 'CreateController@verification');
    
            Route::group(['middleware' => 'auth'], function ($router) {
                $router->pattern('id', '[0-9]+');
                
                Route::get('posts/{id}/edit', 'EditController@getForm');
                Route::put('posts/{id}/edit', 'EditController@postForm');
                //Route::get('posts/{id}/packages', 'PackageController@getForm');
                Route::get('posts/packages', 'SubscriptionsController@getForm');
                Route::post('posts/packages', 'SubscriptionsController@postForm');
                Route::post('posts/{id}/packages', 'PackageController@postForm');
        
                // Payment Gateway Success & Cancel
                Route::get('posts/{id}/payment/success', 'PackageController@paymentConfirmation');
                Route::get('posts/{id}/payment/cancel', 'PackageController@paymentCancel');
            });
	
			// Post's Details
            Route::get(LaravelLocalization::transRoute('routes.post'), 'DetailsController@index');
	
			// Contact Job's Author
            Route::post('posts/{id}/contact', 'DetailsController@sendMessage');
    
            // Send report abuse
            Route::get('posts/{id}/report', 'ReportController@showReportForm');
            Route::post('posts/{id}/report', 'ReportController@sendReport');
        });
        Route::post('send-by-email', 'Search\SearchController@sendByEmail');

        // Profiles
        Route::group(['namespace' => 'Profile'], function($router) {
            $router->pattern('id', '[0-9]+');
            // $router->pattern('slug', '.*');
            $router->pattern('slug', '^(?=.*)((?!\/).)*$');
            
            //Route::get('profile/subscription/create/', 'SubscriptionsController@getForm');
            //Route::post('profile/subscription/create/', 'SubscriptionsController@postForm');
            
            Route::group(['middleware' => 'auth'], function ($router) { 
                //Create Employee Profile Routes {type}
                Route::get('profile/create/{id}', 'CreateController@getForm');
                Route::post('profile/create/{id}', 'CreateController@postForm');
                Route::get('profile/additional/', 'CreateController@getExtraForm');
                Route::post('profile/additional/', 'CreateController@postExtraForm');

                Route::post('profile/create/{token}', 'CreateController@postForm');
                Route::get('profile/finish', 'CreateController@finish');
            });
            /* Route::get('employee-profile/{profile_id}/edit', 'CreateController@getEditForm');
            Route::post('employee-profile/{profile_id}/edit', 'CreateController@postEditForm');
            Route::get('employee-profile/{profile_id}/delete', 'CreateController@deleteProfile'); 
            Route::put('profile/create/{tmpToken}', 'CreateController@postForm');
            Route::get('profile/create/{tmpToken}/packages', 'PackageController@getForm');
            Route::post('profile/create/{tmpToken}/packages', 'PackageController@postForm');
            Route::get('profile/create/{tmpToken}/finish', 'CreateController@finish');
    
            // Email Address or Phone Number verification
            $router->pattern('field', 'email|phone');
            Route::get('verify/profile/{id}/resend/email', 'CreateController@reSendVerificationEmail');
            Route::get('verify/profile/{id}/resend/sms', 'CreateController@reSendVerificationSms');
            Route::get('verify/profile/{field}/{token?}', 'CreateController@verification');
            Route::post('verify/profile/{field}/{token?}', 'CreateController@verification');  */
    
            
            //Editing Employee Profile
            Route::group(['middleware' => 'auth'], function ($router) {
                $router->pattern('id', '[0-9]+');

                /*Route::get('profile/{id}/edit', 'EditController@getForm');
                Route::put('profile/{id}/edit', 'EditController@postForm');*/

                Route::get('profile/{id}/edit-basic', 'EditController@getBasicForm');
                Route::put('profile/{id}/edit-basic', 'EditController@postBasicForm');
                Route::get('profile/{id}/edit-education', 'EditController@getEducationForm');
                Route::put('profile/{id}/edit-education', 'EditController@postEducationForm');
                Route::get('profile/{id}/edit-employment', 'EditController@getEmploymentForm');
                Route::put('profile/{id}/edit-employment', 'EditController@postEmploymentForm');                
                Route::get('profile/{id}/edit-extra', 'EditController@getExtraForm');
                Route::put('profile/{id}/edit-extra', 'EditController@postExtraForm');

                //Ajax calls
                Route::get('profile/{id}/edit-resume', 'EditController@getResumeForm');
                Route::put('profile/{id}/edit-resume', 'EditController@postResumeForm');
                Route::get('profile/{id}/edit-photo', 'EditController@getPhotoForm');
                Route::put('profile/{id}/edit-photo', 'EditController@postPhotoForm');
            });
    
            // Profile's Details
            Route::get(LaravelLocalization::transRoute('routes.profile'), 'DetailsController@index');
    
            // Contact Profile's Author
            Route::post('profile/{id}/contact', 'DetailsController@sendMessage');
    
            // Send report abuse
            Route::get('profile/{id}/report', 'ReportController@showReportForm');
            Route::post('profile/{id}/report', 'ReportController@sendReport');
        });
        //Route::post('send-by-email', 'Search\SearchController@sendByEmail');


        // COMMON ACCOUNT , 'prefix' => 'common' 
        Route::group(['middleware' => ['auth', 'bannedUser', 'preventBackHistory'], 'namespace' => 'CommonAccount' ],  function($router) {
            $router->pattern('id', '[0-9]+');

            // Users
            Route::get('account', 'DashboardController@index');
            Route::get('account/edit', 'EditController@index');

            //Route::get('account', 'EditController@index');
			Route::group(['middleware' => 'impersonate.protect'], function () {
				Route::put('account', 'EditController@updateDetails');
				Route::put('account/settings', 'EditController@updateSettings');
				Route::put('account/preferences', 'EditController@updatePreferences');
			});
			Route::get('account/close', 'CloseController@index');
			Route::group(['middleware' => 'impersonate.protect'], function () {
				Route::post('account/close', 'CloseController@submit');
			});
            
            // Companies
			Route::get('account/companies', 'CompanyController@index');
			Route::get('account/companies/create', 'CompanyController@create');
			Route::post('account/companies', 'CompanyController@store');
			Route::get('account/companies/{id}', 'CompanyController@show');
			Route::get('account/companies/{id}/edit', 'CompanyController@edit');
			Route::put('account/companies/{id}', 'CompanyController@update');
			Route::get('account/companies/{id}/delete', 'CompanyController@destroy');
			Route::post('account/companies/delete', 'CompanyController@destroy');
	
			// Profiles
			/*Route::get('account/resumes', 'ResumeController@index');
			Route::get('account/resumes/create', 'ResumeController@create');
			Route::post('account/resumes', 'ResumeController@store');
			Route::get('account/resumes/{id}', 'ResumeController@show');
			Route::get('account/resumes/{id}/edit', 'ResumeController@edit');
			Route::put('account/resumes/{id}', 'ResumeController@update');
			Route::get('account/resumes/{id}/delete', 'ResumeController@destroy');
			Route::post('account/resumes/delete', 'ResumeController@destroy');*/
			
			// Saved Jobs
            Route::get('account/saved-search', 'PostsController@getSavedSearch');
            $router->pattern('pagePath', '(my-posts|archived|favourite|pending-approval|saved-search)+');
            Route::get('account/{pagePath}', 'PostsController@getPage');
            Route::get('account/{pagePath}/{id}/repost', 'PostsController@getArchivedPosts');
            Route::get('account/{pagePath}/{id}/delete', 'PostsController@destroy');
            Route::post('account/{pagePath}/delete', 'PostsController@destroy');
            Route::get('account/job-list', 'PostsController@jobList');

            Route::get('account/employee-profile-list', 'EmployeeProfileController@getMyEmployeeProfile');
	
			// Conversations
			Route::get('account/conversations', 'ConversationsController@index');
			Route::get('account/conversations/{id}/delete', 'ConversationsController@destroy');
			Route::post('account/conversations/delete', 'ConversationsController@destroy');
			Route::post('account/conversations/{id}/reply', 'ConversationsController@reply');
			$router->pattern('msgId', '[0-9]+');
			Route::get('account/conversations/{id}/messages', 'ConversationsController@messages');
			Route::get('account/conversations/{id}/messages/{msgId}/delete', 'ConversationsController@destroy');
			Route::post('account/conversations/{id}/messages/delete', 'ConversationsController@destroy');
	
			// Transactions
			Route::get('account/transactions', 'TransactionsController@index');

            //Subscriptions
            Route::get('account/subscriptions', 'SubscriptionsController@getPackages'); 
            Route::get('subscription/create/', 'SubscriptionsController@getForm');
            Route::post('subscription/create/', 'SubscriptionsController@postForm');
        });

         // Business ACCOUNT
         /* Route::group([
                    'middleware' => ['auth', 'bannedUser', 'preventBackHistory'], 
                    'prefix'     => 'business',
                    'namespace' => 'BusinessAccount'
        ],  function($router) {
            $router->pattern('id', '[0-9]+');

            // Users
            Route::get('account', 'DashboardController@index');
            Route::get('account/edit', 'EditController@index');

            //Route::get('account', 'EditController@index');
			Route::group(['middleware' => 'impersonate.protect'], function () {
				Route::put('account', 'EditController@updateDetails');
				Route::put('account/settings', 'EditController@updateSettings');
				Route::put('account/preferences', 'EditController@updatePreferences');
			});
			Route::get('account/close', 'CloseController@index');
			Route::group(['middleware' => 'impersonate.protect'], function () {
				Route::post('account/close', 'CloseController@submit');
			});
            
            // Companies
			Route::get('account/companies', 'CompanyController@index');
			Route::get('account/companies/create', 'CompanyController@create');
			Route::post('account/companies', 'CompanyController@store');
			Route::get('account/companies/{id}', 'CompanyController@show');
			Route::get('account/companies/{id}/edit', 'CompanyController@edit');
			Route::put('account/companies/{id}', 'CompanyController@update');
			Route::get('account/companies/{id}/delete', 'CompanyController@destroy');
			Route::post('account/companies/delete', 'CompanyController@destroy');
	
			// Resumes
			Route::get('account/resumes', 'ResumeController@index');
			Route::get('account/resumes/create', 'ResumeController@create');
			Route::post('account/resumes', 'ResumeController@store');
			Route::get('account/resumes/{id}', 'ResumeController@show');
			Route::get('account/resumes/{id}/edit', 'ResumeController@edit');
			Route::put('account/resumes/{id}', 'ResumeController@update');
			Route::get('account/resumes/{id}/delete', 'ResumeController@destroy');
			Route::post('account/resumes/delete', 'ResumeController@destroy');
			
			// Posts
            Route::get('account/saved-search', 'PostsController@getSavedSearch');
            $router->pattern('pagePath', '(my-posts|archived|favourite|pending-approval|saved-search)+');
            Route::get('account/{pagePath}', 'PostsController@getPage');
            Route::get('account/{pagePath}/{id}/repost', 'PostsController@getArchivedPosts');
            Route::get('account/{pagePath}/{id}/delete', 'PostsController@destroy');
            Route::post('account/{pagePath}/delete', 'PostsController@destroy');
            Route::get('account/job-list', 'PostsController@jobList');

              Route::get('account/employee-profile-list', 'EmployeeprofileController@getMyEmployeeProfile');
	
			// Conversations
			Route::get('account/conversations', 'ConversationsController@index');
			Route::get('account/conversations/{id}/delete', 'ConversationsController@destroy');
			Route::post('account/conversations/delete', 'ConversationsController@destroy');
			Route::post('account/conversations/{id}/reply', 'ConversationsController@reply');
			$router->pattern('msgId', '[0-9]+');
			Route::get('account/conversations/{id}/messages', 'ConversationsController@messages');
			Route::get('account/conversations/{id}/messages/{msgId}/delete', 'ConversationsController@destroy');
			Route::post('account/conversations/{id}/messages/delete', 'ConversationsController@destroy');
	
			// Transactions
			Route::get('account/transactions', 'TransactionsController@index');

            //Subscriptions
            Route::get('account/subscriptions', 'SubscriptionsController@getPackages'); 
            Route::get('subscription/create/', 'SubscriptionsController@getForm');
            Route::post('subscription/create/', 'SubscriptionsController@postForm');
        }); */
	
	    //Subscriptions
        /*Route::group(['middleware' => 'auth', 'namespace' => 'Subscriptions'], function($router) {
            Route::get('account/subscriptions', 'SubscriptionsController@index'); 
            Route::get('account/subscription/create/', 'SubscriptionsController@getForm');
            Route::post('account/subscription/create/', 'SubscriptionsController@postForm');


        });*/
        
		// FEEDS
		Route::feeds();


        // Country Code Pattern
        $countryCodePattern = implode('|', array_map('strtolower', array_keys(getCountries())));
        $router->pattern('countryCode', $countryCodePattern);


        // XML SITEMAPS
        Route::get('{countryCode}/sitemaps.xml', 'SitemapsController@site');
        Route::get('{countryCode}/sitemaps/pages.xml', 'SitemapsController@pages');
        Route::get('{countryCode}/sitemaps/categories.xml', 'SitemapsController@categories');
        Route::get('{countryCode}/sitemaps/cities.xml', 'SitemapsController@cities');
        Route::get('{countryCode}/sitemaps/posts.xml', 'SitemapsController@posts');


        // STATICS PAGES
        Route::group(['middleware' => 'httpCache:yes'], function($router) {
            Route::get(LaravelLocalization::transRoute('routes.page'), 'PageController@index');
            Route::get(LaravelLocalization::transRoute('routes.contact'), 'PageController@contact');
            Route::post(LaravelLocalization::transRoute('routes.contact'), 'PageController@contactPost');
            Route::get(LaravelLocalization::transRoute('routes.sitemap'), 'SitemapController@index');
			Route::get(LaravelLocalization::transRoute('routes.companies-list'), 'Search\CompanyController@index');
        });


        // DYNAMIC URL PAGES
        $router->pattern('id', '[0-9]+');
		$router->pattern('username', '[a-zA-Z0-9]+');
        Route::get(LaravelLocalization::transRoute('routes.search'), 'Search\SearchController@index');
        Route::get(LaravelLocalization::transRoute('routes.search-user'), 'Search\UserController@index');
		Route::get(LaravelLocalization::transRoute('routes.search-username'), 'Search\UserController@profile');
        Route::get(LaravelLocalization::transRoute('routes.search-company'), 'Search\CompanyController@profile');
		Route::get(LaravelLocalization::transRoute('routes.search-tag'), 'Search\TagController@index');
        Route::get(LaravelLocalization::transRoute('routes.search-city'), 'Search\CityController@index');
        Route::get(LaravelLocalization::transRoute('routes.search-subCat'), 'Search\CategoryController@index');
        Route::get(LaravelLocalization::transRoute('routes.search-cat'), 'Search\CategoryController@index');

        // DYNAMIC URL PAGES FOR EMPLOYEE PROFILES
        $router->pattern('id', '[0-9]+');
		$router->pattern('username', '[a-zA-Z0-9]+');
        Route::get(LaravelLocalization::transRoute('routes.profile-search'), 'ProfileSearch\SearchController@index');        
		Route::get(LaravelLocalization::transRoute('routes.search-tag'), 'ProfileSearch\TagController@index');
        Route::get(LaravelLocalization::transRoute('routes.search-city'), 'ProfileSearch\CityController@index');
        Route::get(LaravelLocalization::transRoute('routes.search-subCat'), 'ProfileSearch\CategoryController@index');
        Route::get(LaravelLocalization::transRoute('routes.search-cat'), 'ProfileSearch\CategoryController@index');

        
    });
});

//Testing Purpose only
/* Route::get('test',function(){
                
    echo "<pre>"; print_r(App\Models\EmployeeProfileEducation::find(1)->employeeProfile->resume);  echo "</pre>";           

    $photo = 'http://dev.fairconnects.localhost/uploads/'.App\Models\EmployeeProfileEducation::find(1)->employeeProfile->resume;

    echo '<img src="'.$photo.'">';
});
 */
